<?php

namespace Drupal\codev_pages;

use Drupal;
use Drupal\codev_utils\Helper\Media;
use Drupal\Core\Config\Config as ConfigBase;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystem;
use Drupal\file\Entity\File;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Config.php
 * .
 */

/**
 * Class Config.
 *
 * Manage config settings.
 *
 * @package      Drupal\codev_pages
 *
 * @noinspection PhpUnused
 */
class Config {

  /**
   * Return paragraph type config, editable object.
   *
   * @param string $type
   *
   * @return ConfigBase
   */
  public static function getParagraphTypeConfig(string $type): ConfigBase {
    $name = 'paragraphs.paragraphs_type.' . $type;
    return Drupal::configFactory()->getEditable($name);
  }

  /**
   * Check if config type of 'paragraphs_type'. If ist true the function will
   * return the config type.
   *
   * @param ConfigBase $config
   *
   * @return string|false
   */
  public static function isParagraphTypeConfig(ConfigBase $config): bool|string {
    $s = explode('.', $config->getName());
    if (!empty($s[0]) && $s[0] == 'paragraphs'
      && !empty($s[1]) && $s[1] == 'paragraphs_type') {
      return !empty($s[2]) ? $s[2] : FALSE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Create file entity for the paragraph type.
   *
   * @param string $icon_path
   *   Path to the Icon file to set on the paragraph type.
   *
   * @return File|null
   */
  private static function createParagraphTypeIconFile(string $icon_path): ?File {
    try {
      $icon_s = explode('/', $icon_path);
      $icon_name = end($icon_s);
      $destination_path = 'public://paragraphs_type_icon';
      $destination = sprintf('%s/%s', $destination_path, $icon_name);
      /** @var FileSystem $file_system */
      $file_system = Drupal::service('file_system');
      $file_system->prepareDirectory($destination_path, $file_system::CREATE_DIRECTORY);
      $uri = $file_system->copy($icon_path, $destination, FileExists::Replace);
      return File::create(['uri' => $uri]);
    } catch (Exception) {
      return NULL;
    }
  }

  /**
   * Set an icon to the paragraph type config.
   *
   * @param string $paragraph_type
   *   Paragraph type identifier.
   * @param string $icon_path
   *   Path to the Icon file to set on the paragraph type.
   *
   * @return bool
   */
  public static function setParagraphTypeIconDef(string $paragraph_type, string $icon_path): bool {
    $config = static::getParagraphTypeConfig($paragraph_type);
    if (file_exists($icon_path) && !$config->isNew()) {
      try {
        $file = static::createParagraphTypeIconFile($icon_path);
        $config->set('icon_default', Media::buildBase64($file));
        $config->save();
        return TRUE;
      } catch (Exception) {
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Set an icon to the paragraph type config.
   *
   * @param string $paragraph_type
   *   Paragraph type identifier.
   * @param string $icon_path
   *   Path to the Icon file to set on the paragraph type.
   * @param bool   $override_default
   *
   * @return bool
   */
  public static function setParagraphTypeIcon(string $paragraph_type, string $icon_path, bool $override_default = FALSE): bool {
    $config = static::getParagraphTypeConfig($paragraph_type);
    if (file_exists($icon_path) && !$config->isNew()) {
      try {
        $file = static::createParagraphTypeIconFile($icon_path);
        $file->save();
        $dependencies = $config->get('dependencies');
        $dependencies['content'] = ['file:file:' . $file->uuid()];
        $config->set('dependencies', $dependencies);
        $config->set('icon_uuid', $file->uuid());
        $config->save();
        if ($override_default || empty($config->get('icon_default'))) {
          static::setParagraphTypeIconDef($paragraph_type, $icon_path);
        }
        return TRUE;
      } catch (Exception) {
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Set node reference field node types.
   *
   * @param string $type
   *   Node type name.
   *
   * @return bool
   */
  public static function setNodeReferenceFieldNodeType(string $type): bool {
    $config_factory = Drupal::configFactory();
    $block_field = $config_factory->getEditable('field.field.block_content.content_teaser.field_node');
    $paragraph_field = $config_factory->getEditable('field.field.paragraph.content_teaser.field_node');
    if (!$block_field->isNew() && !$paragraph_field->isNew()) {
      try {
        $block_field_data = $block_field->getRawData();
        $paragraph_field_date = $paragraph_field->getRawData();
        $node_key = 'node.type.' . $type;
        if (!in_array($node_key, $block_field_data['dependencies']['config'])) {
          $block_field_data['dependencies']['config'][] = $node_key;
        }
        if (!in_array($node_key, $paragraph_field_date['dependencies']['config'])) {
          $paragraph_field_date['dependencies']['config'][] = $node_key;
        }
        if (empty($block_field_data['settings']['handler_settings']['target_bundles'][$type])) {
          $block_field_data['settings']['handler_settings']['target_bundles'][$type] = $type;
        }
        if (empty($paragraph_field_date['settings']['handler_settings']['target_bundles'][$type])) {
          $paragraph_field_date['settings']['handler_settings']['target_bundles'][$type] = $type;
        }
        $block_field->setData($block_field_data)->save();
        $paragraph_field->setData($paragraph_field_date)->save();
        return TRUE;
      } catch (Exception) {
        return FALSE;
      }
    }
    return FALSE;
  }

}
