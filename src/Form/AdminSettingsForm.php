<?php

namespace Drupal\codev_pages\Form;

use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AdminSettingsForm.php
 * .
 */

/**
 * Class AdminSettingsForm.
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * The layout simply table key
   */
  protected string $simplyLayoutTableKey = 'grid_simply_layouts_table_';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codev_pages_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_pages.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    $form['#attached']['library'][] = 'codev_pages/admin-settings-form';

    // Default layout settings
    $form['default'] = $this->buildDefaultSettingsElements($data);

    // Grid system settings
    $form['grid'] = $this->buildGridSystemSettingsElements($data, $form_state);

    // Simply grid base class grid layout builder settings
    $form['simply_layout'] = $this->buildSimplyLayoutSettingsElements($form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Return the default settings form elements.
   */
  protected function buildDefaultSettingsElements($data): array {
    $ret = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Default layout settings'),
    ];

    $ret['grid_size'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Select max grid size'),
      '#options'       => $this->buildGridSizeOptions(),
      '#default_value' => Utils::getArrayValue('grid_size', $data, ''),
      '#prefix'        => '<div class="container-inline">',
      '#suffix'        => '</div>',
    ];

    return $ret;
  }

  /**
   * Return the grid system settings form elements.
   */
  protected function buildGridSystemSettingsElements($data, $form_state): array {
    $grid_prefix_count = $form_state->get('grid_prefix_count') ?: 0;

    $ret = [
      '#type'  => 'details',
      '#title' => $this->t('Advanced layout settings'),
    ];

    $ret['grid_container_class'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Grid container css class name'),
      '#default_value' => Utils::getArrayValue('grid_container_class', $data, ''),
      '#required'      => TRUE,
    ];

    $ret['grid_gutter_class'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Grid gutter css class name'),
      '#default_value' => Utils::getArrayValue('grid_gutter_class', $data, ''),
      '#required'      => TRUE,
    ];

    $ret['grid_row_class'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('CSS grid row class name'),
      '#default_value' => Utils::getArrayValue('grid_row_class', $data, ''),
      '#required'      => TRUE,
    ];

    $ret['grid_col_class'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('CSS global grid column class name'),
      '#default_value' => Utils::getArrayValue('grid_col_class', $data, ''),
    ];

    $ret['grid_col_prefix'] = [
      '#type'       => 'container',
      '#attributes' => [
        'id' => ['grid-prefix-wrapper'],
      ],
    ];

    if ($form_state->get('grid_col_prefix_unsaved')) {
      $ret['grid_col_prefix']['unsaved'] = $this->unsavedElement();
    }

    $ret['grid_col_prefix']['grid_col_prefix_table'] = [
      '#type'      => 'table',
      '#title'     => $this->t('CSS global grid column class name'),
      '#header'    => [
        '',
        $this->t('Prefix'),
        $this->t('Machine name'),
        $this->t('Weight'),
      ],
      '#empty'     => $this->t('No prefixes found!'),
      '#tabledrag' => [
        [
          'action'       => 'order',
          'relationship' => 'sibling',
          'group'        => 'row-weight',
        ],
      ],
    ];

    foreach ($data['grid_col_prefix'] ?: [] as $key => $value) {
      $ret['grid_col_prefix']['grid_col_prefix_table'][$key] = $this->buildGridTableItem(
        Utils::getArrayValue('id', $value, ''),
        Utils::getArrayValue('prefix', $value, ''),
        $key ?: 0
      );
    }

    $items_count = count($ret['grid_col_prefix']['grid_col_prefix_table']);
    for ($i = 0; $i < $grid_prefix_count; $i++) {
      $weight = $items_count + $i;
      $ret['grid_col_prefix']['grid_col_prefix_table'][] = $this->buildGridTableItem('', '', $weight);
    }

    foreach ($form_state->get('grid_prefix_unset') ?: [] as $id) {
      if (!empty($ret['grid_col_prefix']['grid_col_prefix_table'][$id])) {
        unset($ret['grid_col_prefix']['grid_col_prefix_table'][$id]);
      }
    }

    $ret['add'] = [
      '#type'   => 'submit',
      '#name'   => 'prefix_add',
      '#value'  => $this->t('Add new'),
      '#submit' => ['::addNewGridPrefixItemToTable'],
      '#ajax'   => [
        'callback' => '::updateGridTableElement',
        'wrapper'  => 'grid-prefix-wrapper',
      ],
    ];

    $ret['remove'] = [
      '#type'       => 'submit',
      '#name'       => 'prefix_remove',
      '#value'      => $this->t('Delete'),
      '#submit'     => ['::deleteGridPrefixItemToTable'],
      '#ajax'       => [
        'callback' => '::updateGridTableElement',
        'wrapper'  => 'grid-prefix-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
    ];

    return $ret;
  }

  /**
   * Return the category table item.
   *
   * @param string $id
   * @param string $prefix
   * @param int    $weight
   *
   * @return array
   */
  private function buildGridTableItem(string $id = '', string $prefix = '', int $weight = 0): array {
    return [
      '#weight'     => $weight,
      '#attributes' => ['class' => ['draggable']],
      'delete'      => [
        '#type' => 'checkbox',
      ],
      'prefix'      => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Prefix'),
        '#title_display' => 'invisible',
        '#required'      => TRUE,
        '#default_value' => $prefix,
      ],
      'id'          => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Machine name'),
        '#title_display' => 'invisible',
        '#required'      => TRUE,
        '#default_value' => $id,
        '#disabled'      => !empty($id),
      ],
      'weight'      => [
        '#type'          => 'weight',
        '#title'         => $this->t('Weight for @title', [
          '@title' => $id,
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes'    => [
          'class' => [
            'row-weight',
          ],
        ],
      ],
    ];
  }

  /**
   * Return the simple layout settings elements.
   *
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  private function buildSimplyLayoutSettingsElements(FormStateInterface $form_state): array {
    $simply_layouts = Settings::getGridSimplyLayouts();
    $simply_layouts_select = $form_state->get('simply_layouts_select') ?: [];
    $simply_layouts_select_unset = $form_state->get('simply_layouts_select_unset') ?: [];

    $keys = array_keys($simply_layouts);
    $keys = array_merge($keys, $simply_layouts_select);
    $grid_simply_layouts_select_options = array_combine($keys, $keys);
    asort($grid_simply_layouts_select_options);
    foreach ($grid_simply_layouts_select_options as $val) {
      if (in_array($val, $simply_layouts_select_unset)
        && !empty($grid_simply_layouts_select_options[$val])) {
        unset($grid_simply_layouts_select_options[$val]);
      }
    }

    $ret = [
      '#type'       => 'details',
      '#title'      => $this->t('Layout builder settings: Grid layout simply'),
      '#open'       => TRUE,
      '#attributes' => ['id' => ['layout-selector-wrapper']],
    ];

    if ($form_state->get('simply_layouts_unsaved')) {
      $ret['unsaved'] = $this->unsavedElement();
    }

    $ret['grid_simply_layouts_select'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['layout-selector-columns']],
    ];

    $ret['grid_simply_layouts_select']['grid_simply_layouts_select'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Columns'),
      '#options'       => $grid_simply_layouts_select_options,
      '#default_value' => !empty($simply_layouts_select) ? end($simply_layouts_select) : $keys[0],
    ];

    $ret['grid_simply_layouts_select']['grid_simply_layouts_select_size'] = [
      '#type'          => 'number',
      '#min'           => 1,
      '#max'           => Settings::get('grid_size'),
      '#default_value' => count($grid_simply_layouts_select_options ?: 1),
    ];

    $ret['grid_simply_layouts_select']['add'] = [
      '#type'   => 'submit',
      '#name'   => 'select_add',
      '#value'  => $this->t('Add new'),
      '#submit' => ['::addNewSimplyLayoutSelectItemToTable'],
      '#ajax'   => [
        'callback' => '::updateSimplyLayoutTableElement',
        'wrapper'  => 'layout-selector-wrapper',
      ],
    ];

    $ret['grid_simply_layouts_select']['remove'] = [
      '#type'       => 'submit',
      '#name'       => 'select_remove',
      '#value'      => $this->t('Delete'),
      '#submit'     => ['::deleteSimplyLayoutSelectItemToTable'],
      '#ajax'       => [
        'callback' => '::updateSimplyLayoutTableElement',
        'wrapper'  => 'layout-selector-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
    ];

    foreach ($simply_layouts_select as $column) {
      if (empty($simply_layouts[$column])) {
        $simply_layouts[$column] = [[]];
      }
    }

    foreach ($simply_layouts as $column => $layouts) {
      $ret['grid_simply_layouts'][$column] = $this->buildSimpleLayoutGridSelectorTable($column, $layouts, $form_state);
    }

    return $ret;
  }

  /**
   * Return the simple layout grid selector table elements.
   *
   * @param string             $id
   * @param array              $data
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  private function buildSimpleLayoutGridSelectorTable(string $id, array $data, FormStateInterface $form_state): array {
    $count = 0;
    $simply_layout_columns = $form_state->get('simply_layout_columns') ?: [];
    $simply_layout_columns_unset = $form_state->get('simply_layout_columns_unset') ?: [];
    $simply_layout_columns_count = Utils::getArrayValue($id, $simply_layout_columns, 0);
    $table_id = $this->simplyLayoutTableKey . $id;

    $ret = [
      '#type'   => 'container',
      '#states' => [
        'visible' => [
          ':input[name="grid_simply_layouts_select"]' => ['value' => $id],
        ],
      ],
      $table_id => [
        '#type'      => 'table',
        '#header'    => [
          '',
          $this->t('Columns'),
          $this->t('Weight'),
        ],
        '#empty'     => $this->t('No layouts found!'),
        '#tabledrag' => [
          [
            'action'       => 'order',
            'relationship' => 'sibling',
            'group'        => 'row-weight',
          ],
        ],
      ],
      'add'     => [
        '#type'   => 'submit',
        '#name'   => 'add_' . $table_id,
        '#value'  => $this->t('Add new'),
        '#submit' => ['::addNewSimplyLayoutColumnItemToTable'],
        '#ajax'   => [
          'callback' => '::updateSimplyLayoutTableElement',
          'wrapper'  => 'layout-selector-wrapper',
        ],
      ],
      'remove'  => [
        '#type'       => 'submit',
        '#name'       => 'remove_' . $table_id,
        '#value'      => $this->t('Delete'),
        '#submit'     => ['::deleteSimplyLayoutColumnItemToTable'],
        '#ajax'       => [
          'callback' => '::updateSimplyLayoutTableElement',
          'wrapper'  => 'layout-selector-wrapper',
        ],
        '#attributes' => [
          'class' => [
            'button',
            'button--danger',
          ],
        ],
      ],
    ];

    foreach ($data as $key => $layout) {
      $ret[$table_id][$key] = $this->buildSimpleLayoutGridSelectorTableItem(
        $key, $layout ?: [], $count);
      $count++;
    }


    for ($i = 0; $i < $simply_layout_columns_count; $i++) {
      $ret[$table_id][] = $this->buildSimpleLayoutGridSelectorTableItem($count, [], $count);
      $count++;
    }

    if (!empty($simply_layout_columns_unset[$id])) {
      foreach ($simply_layout_columns_unset[$id] as $unset) {
        if (!empty($ret[$table_id][$unset])) {
          unset($ret[$table_id][$unset]);
        }
      }
    }

    return $ret;
  }

  /**
   * Return the column's selector table item.
   *
   * @param string $key
   * @param array  $data
   * @param int    $weight
   *
   * @return array
   */
  private function buildSimpleLayoutGridSelectorTableItem(string $key, array $data, int $weight = 0): array {
    $columns = [];
    $columns_presets = Settings::get('grid_col_prefix');
    foreach ($columns_presets as $column) {
      if (!empty($column['id'])) {
        $column_id = Utils::getArrayValue('id', $column);
        $column_data = Utils::getArrayValue($column_id, $data, []);
        $columns[$column_id] = [
          '#type'          => 'textfield',
          '#title'         => $this->t('Column @name', [
            '@name' => $column_id,
          ]),
          '#default_value' => Json::encode($column_data),
          '#required'      => $columns_presets[0] === $column,
          '#prefix'        => '<div class="container-inline">',
          '#suffix'        => '</div>',
        ];
      }
    }
    return [
      '#weight'     => $weight,
      '#attributes' => ['class' => ['draggable']],
      'delete'      => [
        '#type' => 'checkbox',
      ],
      'columns'     => $columns,
      'weight'      => [
        '#type'          => 'weight',
        '#title'         => $this->t('Weight for @title', [
          '@title' => $key,
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes'    => [
          'class' => [
            'row-weight',
          ],
        ],
      ],
    ];
  }

  /**
   * Append unsaved message to array.
   */
  private function unsavedElement(): array {
    return [
      '#markup'     => sprintf(
        '<div class="messages messages--warning">%s</div>',
        $this->t('You have unsaved changes.')),
      '#attributes' => [
        'class' => [
          'view-changed',
          'messages',
          'messages--warning',
        ],
      ],
      '#weight'     => -9999,
    ];
  }

  /**
   * Validation handler.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $uniq_array = [];
    $duplicates_array = [];
    $values = $form_state->getValues() ?: [];
    $triggering_element = $form_state->getTriggeringElement();
    $triggering_submit = Utils::getArrayValue('#submit', $triggering_element, []);

    foreach ($form_state->getValue('grid_col_prefix_table') ?: [] as $key => $item) {
      $id = Utils::getArrayValue('id', $item, '');
      if (!in_array($id, $uniq_array)) {
        $uniq_array[] = $id;
      }
      else {
        $duplicates_array[] = [
          'id'  => $id,
          'key' => $key,
        ];
      }
    }

    foreach ($duplicates_array as $val) {
      if (!empty($val['key'])
        && !empty($form['grid']['grid_col_prefix']['grid_col_prefix_table'][$val['key']])) {
        $form_state->setError($form['grid']['grid_col_prefix']['grid_col_prefix_table'][$val['key']]['id'],
          $this->t('An entry with the ID: "%id" exists ready.',
            ['%id' => $val['id']]
          )
        );
      }
    }

    $columns = $this->getColumnTablesFromValues($values);
    foreach ($columns as $key => $val) {
      foreach ($val as $val_id => $data) {
        if (!empty($data['columns']) && is_array($data['columns'])) {
          foreach ($data['columns'] as $column_id => $column) {
            $count = count(Json::decode($column ?: []));
            if (!empty($column) && $count > $key) {
              $table_name = $this->simplyLayoutTableKey . $key;
              $elem = $form['simply_layout']['grid_simply_layouts'][$key][$table_name][$val_id]['columns'][$column_id];
              $form_state->setError($elem, t('Too much columns'));
            }
          }
        }
      }
    }

    if (in_array('::deleteGridPrefixItemToTable', $triggering_submit)
      || in_array('::deleteSimplyLayoutSelectItemToTable', $triggering_submit)
      || in_array('::deleteSimplyLayoutColumnItemToTable', $triggering_submit)) {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->getEditableConfigNames()[0]);

    $grid_col_prefix = $form_state->getValue('grid_col_prefix_table') ?: [];
    $grid_simply_layouts = $this->getColumnTablesFromValues($form_state->getValues());

    foreach ($grid_simply_layouts as &$data) {
      foreach ($data as &$val) {
        if (!empty($val['columns']) && is_array($val['columns'])) {
          $columns = [];
          foreach ($val['columns'] as $column_id => $column) {
            $columns[] = [
              'id'      => $column_id,
              'columns' => Json::decode($column ?: '[]'),
            ];
          }
          $val = $columns;
        }
      }
    }

    $config->set('grid_size', $form_state->getValue('grid_size'))
      ->set('grid_row_class', $form_state->getValue('grid_row_class'))
      ->set('grid_col_class', $form_state->getValue('grid_col_class'))
      ->set('grid_col_prefix', $this->prepareGridPrefixes($grid_col_prefix))
      ->set('grid_container_class', $form_state->getValue('grid_container_class'))
      ->set('grid_gutter_class', $form_state->getValue('grid_gutter_class'))
      ->set('grid_simply_layouts', array_filter($grid_simply_layouts))
      ->save();
  }

  /**
   * Return the current grid element item.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return mixed
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function updateGridTableElement(array $form, FormStateInterface $form_state): mixed {
    return $form['grid']['grid_col_prefix'];
  }

  /**
   * Update grid prefix item count.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewGridPrefixItemToTable(array &$form, FormStateInterface $form_state): void {
    $grid_prefix_count = $form_state->get('grid_prefix_count') ?: 0;
    $form_state->set('grid_col_prefix_unsaved', TRUE);
    $form_state->set('grid_prefix_count', $grid_prefix_count + 1);
    $form_state->setRebuild();
  }

  /**
   * Remove grid prefix item count.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteGridPrefixItemToTable(array &$form, FormStateInterface $form_state): void {
    $items = $form_state->get('grid_prefix_unset') ?: [];
    foreach ($form_state->getValue('grid_col_prefix_table') ?: [] as $key => $item) {
      if ($item['delete'] && !in_array($key, $items)) {
        $items[] = $key;
      }
    }
    $form_state->set('grid_col_prefix_unsaved', TRUE);
    $form_state->set('grid_prefix_unset', $items);
    $form_state->setRebuild();
  }

  /**
   * Return the simple layout element item.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return mixed
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function updateSimplyLayoutTableElement(array $form, FormStateInterface $form_state): mixed {
    return $form['simply_layout'];
  }

  /**
   * Update simply layout select items.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewSimplyLayoutSelectItemToTable(array &$form, FormStateInterface $form_state): void {
    $layouts_select_size = $form_state->getValue('grid_simply_layouts_select_size') ?: [];
    if (!empty($layouts_select_size)) {
      $selected_columns = $form_state->get('simply_layouts_select') ?: [];
      $selected_columns_unset = $form_state->get('simply_layouts_select_unset') ?: [];
      if (!in_array($layouts_select_size, $selected_columns)) {
        $selected_columns[] = $layouts_select_size;
      }
      foreach ($selected_columns as $key => $column) {
        if ($column === $layouts_select_size) {
          unset($selected_columns_unset[$key]);
        }
      }
      $form_state->set('simply_layouts_select', $selected_columns);
      $form_state->set('simply_layouts_select_unset', $selected_columns_unset);
      $form_state->set('simply_layouts_unsaved', TRUE);
    }
    $form_state->setRebuild();
  }

  /**
   * Remove simply layout select items.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteSimplyLayoutSelectItemToTable(array &$form, FormStateInterface $form_state): void {
    $layouts_select_size = $form_state->getValue('grid_simply_layouts_select') ?: [];
    $items = $form_state->get('simply_layouts_select_unset') ?: [];
    if (!in_array($layouts_select_size, $items)) {
      $items[] = $layouts_select_size;
    }
    $form_state->set('simply_layouts_select_unset', $items);
    $form_state->set('simply_layouts_unsaved', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Update simply layout columns items.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewSimplyLayoutColumnItemToTable(array &$form, FormStateInterface $form_state): void {
    $simply_layout_columns = $form_state->get('simply_layout_columns') ?: [];
    $triggering_element = $form_state->getTriggeringElement();
    $name_s = explode('_', $triggering_element['#name']);
    $column_id = end($name_s);
    if (!empty($simply_layout_columns[$column_id])) {
      $simply_layout_columns[$column_id]++;
    }
    else {
      $simply_layout_columns[$column_id] = 1;
    }
    $form_state->set('simply_layout_columns', $simply_layout_columns);
    $form_state->setRebuild();
  }

  /**
   * Remove simply layout columns items.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteSimplyLayoutColumnItemToTable(array &$form, FormStateInterface $form_state): void {
    $simply_layout_columns_unset = $form_state->get('simply_layout_columns_unset') ?: [];
    $values = $this->getColumnTablesFromValues($form_state->getValues());
    $triggering_element = $form_state->getTriggeringElement();
    $name_s = explode('_', $triggering_element['#name']);
    $column_id = end($name_s);
    if (!empty($values[$column_id])) {
      foreach ($values[$column_id] as $key => $value) {
        $items = Utils::getArrayValue($column_id, $simply_layout_columns_unset, []);
        if (!empty($value['delete'])) {
          $items[] = $key;
        }
        $simply_layout_columns_unset[$column_id] = array_unique($items);
      }
    }
    $form_state->set('simply_layout_columns_unset', $simply_layout_columns_unset);
    $form_state->setRebuild();
  }

  /**
   * Generate and return int array with possible grid sizes.
   *
   * @return array
   */
  private function buildGridSizeOptions(): array {
    $ret = [];
    for ($i = 2; $i <= 24; $i += 2) {
      $ret[$i] = $i;
    }
    return $ret;
  }

  /**
   * Return all layout columns tables by given form values.
   *
   * @param array $values
   *
   * @return array
   */
  private function getColumnTablesFromValues(array $values): array {
    $ret = [];
    foreach ($values as $key => $val) {
      if (strpos($key, $this->simplyLayoutTableKey) === 0) {
        $key_s = explode('_', $key);
        $column_id = end($key_s);
        $ret[$column_id] = $val;
      }
    }
    return $ret;
  }

  /**
   * Prepare grid prefix values for save.
   *
   * @param array $values
   *
   * @return array
   */
  private function prepareGridPrefixes(array $values): array {
    $ret = [];
    if (!empty($values)) {
      Utils::sortArrayByKey($values);
      foreach ($values as $val) {
        $ret[] = [
          'id'     => $val['id'],
          'prefix' => $val['prefix'],
        ];
      }
    }
    return $ret;
  }

}
