<?php

namespace Drupal\codev_pages\Form;

use Drupal\codev_pages\NestedSectionManger;
use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_restrictions\Form\MoveBlockForm as MoveBlockFormBase;

/**
 * Provides a form for moving a block.
 */
class MoveBlockForm extends MoveBlockFormBase {

  /**
   * Third party setting provider.
   *
   * @var string
   */
  private string $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $uuid = NULL): array {
    $form = parent::buildForm($form, $form_state, $section_storage, $delta, $region, $uuid);
    $component_list = [];
    $parent_section = $this->sectionStorage->getSection($delta);
    $parent_uuid = $parent_section->getThirdPartySetting($this->provider, 'uuid');

    foreach ($this->sectionStorage->getSections() as $delta => $section) {
      $settings = $section->getThirdPartySettings($this->provider);
      $uuid = Utils::getArrayValue('parent', $settings);
      if ($parent_uuid === $uuid) {
        $delta = $delta + 1;
        $weight = Utils::getArrayValue('weight', $settings, 0);
        $form['components_wrapper']['components'][$settings['uuid']] = [
          '#attributes' => [
            'class' => [
              'draggable',
              'layout-builder-components-table__row',
            ],
          ],
          'label'       => [
            '#wrapper_attributes' => ['layout-builder-components-table__block-label'],
            '#markup'             => $this->t('Section @section', [
              '@section' => $delta,
            ]),
          ],
          'weight'      => [
            '#type'          => 'weight',
            '#default_value' => $weight,
            '#title'         => $this->t('Weight for @section section', [
              '@section' => $delta,
            ]),
            '#title_display' => 'invisible',
            '#attributes'    => [
              'class' => ['table-sort-weight'],
            ],
          ],
        ];
      }
    }

    foreach (Element::children($form['components_wrapper']['components']) as $uuid) {
      $component_list[$uuid] = $form['components_wrapper']['components'][$uuid];
      unset($form['components_wrapper']['components'][$uuid]);
    }

    if (uasort($component_list, [$this, 'sortComponentList'])) {
      $form['components_wrapper']['components'] += $component_list;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    $section = $this->sectionStorage->getSection($this->delta);
    $sections = [];
    $components = $form_state->getValue('components');
    $accept_components = array_keys($section->getComponents());
    foreach ($components as $uuid => $component) {
      if (!in_array($uuid, $accept_components)) {
        $sections[$uuid] = $component;
        unset($components[$uuid]);
      }
    }
    $form_state->setValue('sections', $sections);
    $form_state->setValue('components', $components);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $sections = $form_state->getValue('sections');
    foreach ($sections as $uuid => $val) {
      if ($section = NestedSectionManger::getSectionByUuid($uuid, $this->sectionStorage)) {
        $section->setThirdPartySetting($this->provider, 'weight', $val['weight']);
      }
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Sort component list
   *
   * @param array $a
   * @param array $b
   *
   * @return int
   */
  private function sortComponentList(array $a, array $b): int {
    $val_a = $a['weight']['#default_value'];
    $val_b = $b['weight']['#default_value'];
    if ($val_a == $val_b) {
      return 0;
    }
    return $val_a < $val_b ? -1 : 1;
  }

}
