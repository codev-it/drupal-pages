<?php

namespace Drupal\codev_pages\Form;

use Drupal\codev_pages\NestedSectionManger;
use Drupal\codev_pages\Settings;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\ConfigureSectionForm;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Provides a form for configuring a layout section.
 */
class ConfigureNestedSectionForm extends ConfigureSectionForm {

  /**
   * Section parent delta.
   *
   * @var int|null
   */
  private ?int $parent;

  /**
   * Section region.
   *
   * @var string
   */
  private string $region;

  /**
   * Third party setting provider.
   *
   * @var string
   */
  private string $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $plugin_id = NULL, $region = NULL): array {
    $this->parent = $delta;
    $this->region = $region;
    $this->sectionStorage = $section_storage;
    $delta_nested = $this->buildSectionNestedDelta($delta);
    return parent::buildForm($form, $form_state, $section_storage, $delta_nested, $plugin_id);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $parent_uuid = $this->getSectionUuid($this->parent);
    $weight = $this->getLastComponentAndSectionWeight($this->parent);
    $section = $this->sectionStorage->getSection($this->delta);
    $section->setThirdPartySetting($this->provider, 'weight', $weight + 1);
    $section->setThirdPartySetting($this->provider, 'parent', $parent_uuid);
    $section->setThirdPartySetting($this->provider, 'region', $this->region);
    $this->layoutTempstoreRepository->set($this->sectionStorage);
  }

  /**
   * Build the nested delta infos.
   *
   * @param int $delta
   *
   * @return int
   */
  private function buildSectionNestedDelta(int $delta): int {
    $count = $delta + 1;
    $parent_uuid = $this->getSectionUuid($delta);
    $mapping = NestedSectionManger::buildSectionMapping($this->sectionStorage);
    foreach ($mapping as $item => $parent) {
      if ($parent == $parent_uuid) {
        $count = $this->buildSectionStorageNestedDeltaRecursive($count, $item, $mapping);
        $count++;
      }
    }
    return $count;
  }

  /**
   * Build the nested delta infos recursive.
   *
   * @param int    $delta
   * @param string $parent_uuid
   * @param array  $mapping
   *
   * @return int
   */
  private function buildSectionStorageNestedDeltaRecursive(int $delta, string $parent_uuid, array $mapping): int {
    $count = $delta;
    foreach ($mapping as $item => $parent) {
      if ($parent == $parent_uuid) {
        $count = $this->buildSectionStorageNestedDeltaRecursive($count, $item, $mapping);
        $count++;
      }
    }
    return $count;
  }

  /**
   * Return the uuid for the selected section.
   *
   * @param int $delta
   *
   * @return string
   */
  private function getSectionUuid(int $delta): string {
    $parent = $this->sectionStorage->getSection($delta);
    return $parent->getThirdPartySetting($this->provider, 'uuid') ?: '';
  }

  /**
   * Get the weight from the current last component element.
   *
   * @param int $delta
   *
   * @return int
   */
  private function getLastComponentAndSectionWeight(int $delta): int {
    $ret = 0;
    $parent_section = $this->sectionStorage->getSection($delta);
    $parent_uuid = $this->getSectionUuid($delta);
    $components = $parent_section->getComponents();

    foreach ($this->sectionStorage->getSections() as $item) {
      $settings = $item->getThirdPartySettings($this->provider);
      if ($settings['parent'] === $parent_uuid) {
        $components[$settings['uuid']] = ['weight' => $settings['weight']];
      }
    }

    foreach ($components as $component) {
      $weight = 0;
      if ($component instanceof SectionComponent) {
        $weight = $component->getWeight();
      }
      elseif (!empty($component['weight'])) {
        $weight = $component['weight'];
      }
      if ($weight > $ret) {
        $ret = $weight;
      }
    }

    return $ret;
  }

}
