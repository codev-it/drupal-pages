<?php

/**
 * @file
 * Contains \Drupal\codev_pages\Helper\Colorbox.
 */

namespace Drupal\codev_pages\Helper;

use Drupal\Core\Field\FieldItemList;
use Drupal\paragraphs\Entity\Paragraph as ParagraphE;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Paragraph.php
 * .
 */

/**
 * Class Paragraph.
 *
 * @package Drupal\codev_pages\Helper
 */
class Paragraph {

  /**
   * Parse all text from paragraph entity.
   *
   * @param ParagraphE $paragraph
   *   Paragraph entity.
   *
   * @return string
   *   Return first text field trimmed and without html tags.
   */
  public static function parseText(ParagraphE $paragraph): string {
    try {
      $ret = '';
      $exclude = ['image', 'section'];
      if (!in_array($paragraph->getType(), $exclude)) {
        /** @var FieldItemList $field */
        foreach ($paragraph->getIterator() as $field) {
          if (empty($ret)) {
            $type = $field->getFieldDefinition()->getType();
            $include_types = [
              'text',
              'text_long',
              'text_with_summary',
            ];
            if (in_array($type, $include_types)) {
              if (!empty($field->getValue()[0]['value'])) {
                $ret = strip_tags(trim($field->getValue()[0]['value']));
              }
            }
          }
        }
      }
      return $ret;
    } catch (Exception) {
      return '';
    }
  }

  /**
   * Create summery off the paragraph field values.
   *
   * @param ParagraphE[] $paragraphs
   *   Paragraph entities array.
   *
   * @return string
   *   Return first text field trimmed and without html tags.
   */
  public static function parseTextMultiple(array $paragraphs): string {
    $ret = '';
    foreach ($paragraphs as $paragraph) {
      $ret .= static::parseText($paragraph);
    }
    return $ret;
  }

}
