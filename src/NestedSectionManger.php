<?php

namespace Drupal\codev_pages;

use Drupal\codev_utils\Helper\Utils;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: NestedSectionManger.php
 * .
 */

/**
 * Class NestedSectionManger.
 *
 * @package      Drupal\codev_pages
 */
class NestedSectionManger {

  /**
   * Search section by uuid from current section storage.
   *
   * @param string|null             $uuid
   * @param SectionStorageInterface $section_storage
   *
   * @return Section|null
   */
  public static function getSectionByUuid(?string $uuid, SectionStorageInterface $section_storage): ?Section {
    if (empty($uuid)) {
      return NULL;
    }
    $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;
    foreach ($section_storage->getSections() as $section) {
      $section_uuid = $section->getThirdPartySetting($provider, 'uuid');
      if ($uuid === $section_uuid) {
        return $section;
      }
    }
    return NULL;
  }

  /**
   * Build section mapping.
   *
   * @param SectionStorageInterface $section_storage
   *
   * @return array
   */
  public static function buildSectionMapping(SectionStorageInterface $section_storage): array {
    $ret = [];
    $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;
    foreach ($section_storage->getSections() as $section) {
      $settings = $section->getThirdPartySettings($provider);
      if (!empty($settings['uuid']) && isset($settings['parent'])) {
        $ret[$settings['uuid']] = $settings['parent'];
      }
    }
    return $ret;
  }

  /**
   * Build all items with current weight.
   *
   * @param int                                            $delta
   * @param SectionStorageInterface $section_storage
   * @param string|null                                    $exclude_uuid
   *
   * @return array
   */
  public static function buildItemsWeightInfos(int $delta, SectionStorageInterface $section_storage, ?string $exclude_uuid = NULL): array {
    $ret = [];
    $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;
    $section = $section_storage->getSection($delta);
    $section_uuid = $section->getThirdPartySetting($provider, 'uuid');
    $components = $section->getComponents();

    foreach ($components as $component) {
      $ret[$component->getUuid()] = [
        'type'   => 'component',
        'uuid'   => $component->getUuid(),
        'weight' => $component->getWeight(),
      ];
    }

    foreach ($section_storage->getSections() as $nested_section) {
      $nested_uuid = $nested_section->getThirdPartySetting($provider, 'uuid');
      $nested_parent = $nested_section->getThirdPartySetting($provider, 'parent');
      if ($section_uuid == $nested_parent) {
        $ret[$nested_uuid] = [
          'type'   => 'section',
          'uuid'   => $nested_uuid,
          'weight' => $nested_section->getThirdPartySetting($provider, 'weight'),
        ];
      }
    }

    if (!empty($exclude_uuid) && $ret[$exclude_uuid]) {
      unset($ret[$exclude_uuid]);
    }
    Utils::sortArrayByKey($ret, 'weight', FALSE);
    return $ret;
  }

  /**
   * Return the current item index by uuid.
   *
   * @param string $uuid
   * @param array  $infos
   *
   * @return int|null
   */
  public static function getItemsWeightInfosIndex(string $uuid, array $infos = []): ?int {
    foreach ($infos as $key => $info) {
      if (!empty($info['uuid']) && $info['uuid'] == $uuid) {
        return $key;
      }
    }
    return 0;
  }

  /**
   * Return the current item index by uuid.
   *
   * @param int   $index
   * @param array $additional
   * @param array $infos
   *
   * @return array
   */
  public static function appendItemsWeightInfos(int $index, array $additional = [], array &$infos = []): array {
    if ($index == 0) {
      array_unshift($infos, $additional);
    }
    elseif ($index > count($infos) - 1) {
      $infos[] = $additional;
    }
    else {
      $new_ret = [];
      foreach ($infos as $key => $item) {
        if ($key == $index) {
          $new_ret[] = $additional;
        }
        $new_ret[] = $item;
      }
      $infos = $new_ret;
    }

    foreach ($infos as $key => &$item) {
      $item['weight'] = $key;
    }

    return $infos;
  }

  /**
   * Update the weight infos for section and components by given an info array.
   *
   * @param array                                          $infos
   * @param Section $section
   * @param SectionStorageInterface $section_storage
   */
  public static function updateItemsWeightInfos(array $infos, Section $section, SectionStorageInterface $section_storage): void {
    $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;
    foreach ($infos as $info) {
      $uuid = $info['uuid'];
      $weight = $info['weight'];
      switch ($info['type']) {
        case 'section';
          if ($item = NestedSectionManger::getSectionByUuid($uuid, $section_storage)) {
            $item->setThirdPartySetting($provider, 'weight', $weight);
          }
          break;
        case 'component':
          $component = $section->getComponent($uuid);
          $component->setWeight($weight);
          break;
      }
    }
  }

}
