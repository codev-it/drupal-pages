<?php

namespace Drupal\codev_pages\Element;

use Drupal\codev_pages\NestedSectionManger;
use Drupal\codev_pages\Settings;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\layout_builder\Element\LayoutBuilder;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: LayoutBuilderExtend.php
 * .
 */

/**
 * Defines a render element for building the Layout Builder UI.
 *
 * @noinspection PhpUnused
 */
class LayoutBuilderExtend extends LayoutBuilder {

  /**
   * Third party setting provider.
   */
  private string $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;

  /**
   * The layout manager.
   *
   * @var LayoutPluginManagerInterface
   */
  protected LayoutPluginManagerInterface $layoutManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): LayoutBuilder|LayoutBuilderExtend|ContainerFactoryPluginInterface|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition, $container->get('event_dispatcher'));
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->layoutManager = $container->get('plugin.manager.core.layout');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  protected function layout(SectionStorageInterface $section_storage): array {
    $this->prepareLayout($section_storage);

    $count = 0;
    $output = [];
    $nested_sections = [];
    $output['#attached']['library'][] = 'layout_builder/drupal.layout_builder';
    $output['#attached']['library'][] = 'codev_pages/layout-builder';

    if ($this->isAjax()) {
      $output['status_messages'] = [
        '#type' => 'status_messages',
      ];
    }

    $output['#attached']['drupalSettings']['path']['currentPathIsAdmin'] = TRUE;
    $output['#type'] = 'container';
    $output['#attributes']['id'] = 'layout-builder';
    $output['#attributes']['class'][] = 'layout-builder';
    $output['#attributes']['class'][] = 'js-layout-builder-region-section';
    $output['#cache']['max-age'] = 0;

    $mapping = NestedSectionManger::buildSectionMapping($section_storage);
    for ($i = 0; $i < $section_storage->count(); $i++) {
      $uuid = $this->getSectionUuid($section_storage, $count);
      if (empty($mapping[$uuid])) {
        $output[] = $this->buildAddSectionLink($section_storage, $count);
        $output[] = $this->buildAdministrativeSection($section_storage, $count);
      }
      else {
        $parent = $mapping[$uuid];
        $section = $section_storage->getSection($count);
        $nested_sections[$parent][$count] = $section;
      }
      $count++;
    }
    $output[] = $this->buildAddSectionLink($section_storage, $count);

    $this->buildNestedSections($output, $nested_sections, $section_storage);

    return $output;
  }

  /**
   * {@inheritDoc}
   */
  protected function buildAdministrativeSection(SectionStorageInterface $section_storage, $delta): array {
    $build = parent::buildAdministrativeSection($section_storage, $delta);

    $section = $section_storage->getSection($delta);
    $layout = $section->getLayout();
    $layout_definition = $layout->getPluginDefinition();
    $section_move_url = Url::fromRoute('codev_pages.move_section', [
      'section_storage_type' => $section_storage->getStorageType(),
      'section_storage'      => $section_storage->getStorageId(),
    ]);

    $uuid = $this->getSectionUuid($section_storage, $delta);
    $markup = '<span class="layout-builder__draggable-section"></span>';
    $build['draggable'] = ['#markup' => Markup::create($markup)];
    $build['#attributes']['data-layout-block-uuid'] = $uuid;
    $build['#attributes']['data-layout-update-section-url'][] = $section_move_url->toString();
    foreach ($layout_definition->getRegions() as $region => $info) {
      if (!empty($build['layout-builder__section'][$region])) {
        $section_link = $this->buildAddSectionLink($section_storage, $delta, $region);
        $build['layout-builder__section'][$region]['#attributes']['class'][] = 'layout-builder-region-section';
        $build['layout-builder__section'][$region]['#attributes']['class'][] = 'js-layout-builder-region-section';
        $build['layout-builder__section'][$region]['layout_builder_add_block']['section'] = $section_link['link'];
      }
    }

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  protected function buildAddSectionLink(SectionStorageInterface $section_storage, $delta, $region = NULL): array {
    $link = parent::buildAddSectionLink($section_storage, $delta);
    /** @var Url $url */
    $url = $link['link']['#url'];
    $route = 'layout_builder.choose_section';
    $definitions = $this->layoutManager
      ->getFilteredDefinitions('layout_builder', [], [
        'section_storage' => $section_storage,
      ]);
    $plugin_id = count($definitions) == 1 ? array_keys($definitions)[0] : NULL;
    $params = [
      'section_storage_type' => $section_storage->getStorageType(),
      'section_storage'      => $section_storage->getStorageId(),
      'delta'                => $delta,
    ];

    if (empty($region) && !empty($plugin_id)) {
      $route = 'layout_builder.configure_section';
      $params['plugin_id'] = $plugin_id;
    }
    elseif (!empty($region)) {
      $params['region'] = $region;
      if (!empty($plugin_id)) {
        $route = 'codev_pages.configure_nested_section';
        $params['plugin_id'] = $plugin_id;
      }
      else {
        $route = 'codev_pages.choose_nested_section';
      }
    }

    $link['link']['#url'] = Url::fromRoute($route,
      array_filter($params, function ($item) {
        return isset($item);
      }), $url->getOptions());

    return $link;
  }

  /**
   * Build the nested sections.
   *
   * @param array                   $output
   * @param array                   $sections
   * @param SectionStorageInterface $section_storage
   */
  protected function buildNestedSections(array &$output, array $sections, SectionStorageInterface $section_storage): void {
    foreach ($output as $key => $item) {
      if (!empty($item['layout-builder__section'])) {
        $output[$key] = $this->buildNestedSectionsRecursive($item, $sections, $section_storage);
      }
    }
  }

  /**
   * Build the nested sections.
   *
   * @param array                   $item
   * @param array                   $sections
   * @param SectionStorageInterface $section_storage
   *
   * @return array
   */
  protected function buildNestedSectionsRecursive(array $item, array &$sections, SectionStorageInterface $section_storage): array {
    if (!empty($item['layout-builder__section'])) {
      $uuid = $item['#attributes']['data-layout-block-uuid'];
      if (!empty($sections[$uuid])) {
        /** @var Section[] $section_array */
        $section_array = $sections[$uuid];
        foreach ($section_array as $delta => $section) {
          $section_uuid = $section->getThirdPartySetting($this->provider, 'uuid');
          $section_region = $section->getThirdPartySetting($this->provider, 'region');
          $section_weight = $section->getThirdPartySetting($this->provider, 'weight') ?: 0;
          $section_build = $this->buildAdministrativeSection($section_storage, $delta);
          $section_build['#weight'] = $section_weight;
          $item['layout-builder__section'][$section_region][$section_uuid] = $section_build;
          if (!empty($sections[$section_uuid])) {
            $elem = $item['layout-builder__section'][$section_region][$section_uuid];
            $recursive = $this->buildNestedSectionsRecursive($elem, $sections, $section_storage);
            $item['layout-builder__section'][$section_region][$section_uuid] = $recursive;
          }
        }
      }
    }
    return $item;
  }

  /**
   * Return the uuid form the section entity.
   *
   * @param SectionStorageInterface $section_storage
   * @param int                     $delta
   *
   * @return string
   */
  private function getSectionUuid(SectionStorageInterface $section_storage, int $delta): string {
    $section = $section_storage->getSection($delta);
    return $section->getThirdPartySetting($this->provider, 'uuid') ?: '';
  }

}
