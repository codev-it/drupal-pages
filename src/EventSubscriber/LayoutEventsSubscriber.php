<?php

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: LayoutEventsSubscriber.php
 * .
 */

namespace Drupal\codev_pages\EventSubscriber;

use Drupal\codev_pages\Settings;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\layout_builder\Event\PrepareLayoutEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package      Drupal\custom_events\EventSubscriber
 *
 * @noinspection PhpUnused
 */
class LayoutEventsSubscriber implements EventSubscriberInterface {

  /**
   * The layout tempstore repository.
   *
   * @var LayoutTempstoreRepositoryInterface
   */
  protected LayoutTempstoreRepositoryInterface $layoutTempstoreRepository;

  /**
   * The UUID generator.
   *
   * @var UuidInterface
   */
  protected UuidInterface $uuidGenerator;

  /**
   * LayoutEventsSubscriber constructor.
   *
   * @param LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   * @param UuidInterface $uuid
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository, UuidInterface $uuid) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
    $this->uuidGenerator = $uuid;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents(): array {
    return [LayoutBuilderEvents::PREPARE_LAYOUT => 'onPrepareLayout'];
  }

  /**
   * React to a config object being saved.
   *
   * @param PrepareLayoutEvent $event
   *   Config crud event.
   */
  public function onPrepareLayout(PrepareLayoutEvent $event): void {
    $section_storage = $event->getSectionStorage();
    $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;
    foreach ($section_storage->getSections() as $section) {
      if (empty($section->getThirdPartySetting($provider, 'uuid'))) {
        $section->setThirdPartySetting($provider, 'uuid', $this->uuidGenerator->generate());
      }
    }
    $this->layoutTempstoreRepository->set($section_storage);
  }

}
