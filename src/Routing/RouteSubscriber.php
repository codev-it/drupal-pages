<?php

namespace Drupal\codev_pages\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 *
 * @noinspection PhpUnused
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('layout_builder.move_block_form')) {
      $route->setDefault('_form', '\Drupal\codev_pages\Form\MoveBlockForm');
    }

    if ($route = $collection->get('layout_builder.move_block')) {
      $route->setDefault('_controller', '\Drupal\codev_pages\Controller\MoveBlockController::build');
    }

    if ($route = $collection->get('layout_builder.choose_inline_block')) {
      $route->setDefault('_controller', '\Drupal\codev_pages\Controller\ChooseBlockController::inlineBlockList');
    }
  }

}
