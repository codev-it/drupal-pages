<?php

namespace Drupal\codev_pages\Controller;

use Drupal\codev_pages\NestedSectionManger;
use Drupal\codev_pages\Settings;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\layout_builder\Controller\MoveBlockController as MoveBlockControllerBase;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Defines a controller to move a block.
 *
 * @noinspection PhpUnused
 */
class MoveBlockController extends MoveBlockControllerBase {

  /**
   * Third party setting provider.
   *
   * @var string
   */
  private string $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;

  /**
   * {@inheritDoc}
   */
  public function build(SectionStorageInterface $section_storage, int $delta_from, int $delta_to, $region_to, $block_uuid, $preceding_block_uuid = NULL): AjaxResponse {
    $delta = $delta_from !== $delta_to ? $delta_to : $delta_from;
    $preceding_uuid = $preceding_block_uuid;
    $curr_weight_info = NestedSectionManger::buildItemsWeightInfos($delta, $section_storage, $block_uuid);
    $preceding_section = NestedSectionManger::getSectionByUuid($preceding_block_uuid, $section_storage);
    if ($preceding_section) {
      $preceding_uuid = $preceding_section->getThirdPartySetting($this->provider, 'uuid');
      $preceding_block_uuid = NULL;
    }

    parent::build($section_storage, $delta_from, $delta_to, $region_to, $block_uuid, $preceding_block_uuid);

    $index = NestedSectionManger::getItemsWeightInfosIndex($preceding_uuid ?: '', $curr_weight_info);
    $append_index = $preceding_uuid ? $index + 1 : 0;
    NestedSectionManger::appendItemsWeightInfos($append_index, [
      'type' => 'component',
      'uuid' => $block_uuid,
    ], $curr_weight_info);
    if ($this->hasSectionInfo($curr_weight_info)) {
      $section = $section_storage->getSection($delta);
      NestedSectionManger::updateItemsWeightInfos($curr_weight_info, $section, $section_storage);
    }

    $this->layoutTempstoreRepository->set($section_storage);
    return $this->rebuildLayout($section_storage);
  }

  /**
   * Check if section info type exists.
   *
   * @param array $arr
   *
   * @return bool
   */
  private function hasSectionInfo(array $arr): bool {
    foreach ($arr as $item) {
      if (!empty($item['type']) && $item['type'] == 'section') {
        return TRUE;
      }
    }
    return FALSE;
  }

}
