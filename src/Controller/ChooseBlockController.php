<?php

namespace Drupal\codev_pages\Controller;

use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_restrictions\Controller\ChooseBlockController as ChooseBlockControllerBase;

/**
 * Defines a controller to choose a new block.
 *
 * @noinspection PhpUnused
 */
class ChooseBlockController extends ChooseBlockControllerBase {

  /**
   * {@inheritDoc}
   */
  public function inlineBlockList(SectionStorageInterface $section_storage, $delta, $region): array {
    $build = parent::inlineBlockList($section_storage, $delta, $region);
    $build['back_button']['#attributes']['class'][] = 'button';
    $build['back_button']['#attributes']['class'][] = 'expanded';
    return $build;
  }

}
