<?php

namespace Drupal\codev_pages\Controller;

use Drupal\Core\Url;
use Drupal\layout_builder\Controller\ChooseSectionController;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Defines a controller to choose a new section.
 *
 * @noinspection PhpUnused
 */
class ChooseNestedSectionController extends ChooseSectionController {

  /**
   * Choose a layout plugin to add as a section.
   *
   * @param SectionStorageInterface $section_storage
   *   The section storage.
   * @param int                                            $delta
   *   The delta of the section to splice.
   *
   * @return array
   *   The render arrays.
   */
  public function build(SectionStorageInterface $section_storage, int $delta, ?string $region = NULL): array {
    $output = parent::build($section_storage, $delta);
    if (!empty($output['layouts']['#items'])) {
      foreach ($output['layouts']['#items'] as &$item) {
        /** @var Url $url */
        $url = $item['#url'];
        $params = $url->getRouteParameters();
        $params['region'] = $region;
        $item['#url'] = Url::fromRoute(
          'codev_pages.configure_nested_section',
          $params, $url->getOptions());
      }
    }
    return $output;
  }

}
