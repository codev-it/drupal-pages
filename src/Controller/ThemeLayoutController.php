<?php

namespace Drupal\codev_pages\Controller;

use Drupal;
use Drupal\codev_pages\Settings;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplayStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThemeLayoutController.
 *
 * @noinspection PhpUnused
 */
class ThemeLayoutController extends ControllerBase {

  /**
   * ThemeHandlerInterface definition.
   *
   * @var ThemeHandlerInterface
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * EntityInterface definition.
   *
   * @var EntityInterface[]
   */
  protected array $nodeTypes;

  /**
   * LayoutBuilderEntityViewDisplayStorage definition.
   *
   * @var LayoutBuilderEntityViewDisplayStorage
   */
  protected LayoutBuilderEntityViewDisplayStorage $entityViewDisplay;

  /**
   * Url definition.
   *
   * @var Url
   */
  protected Url $destinationUrl;

  /**
   * ThemeLayoutController constructor.
   *
   */
  public function __construct(EntityTypeManager $entityTypeManager, ThemeHandlerInterface $themeHandler, RouteMatchInterface $routeMatch) {
    $this->themeHandler = $themeHandler;
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->nodeTypes = $entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
    if (!empty($this->nodeTypes[Settings::THEME_LAYOUT_NODE_TYPE])) {
      unset($this->nodeTypes[Settings::THEME_LAYOUT_NODE_TYPE]);
    }
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->entityViewDisplay = $entityTypeManager
      ->getStorage('entity_view_display');
    $this->destinationUrl = Url::fromRoute($routeMatch->getRouteName());
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): ThemeLayoutController {
    /** @noinspection PhpParamsInspection */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('theme_handler'),
      $container->get('current_route_match')
    );
  }

  /**
   * Theme builder index page.
   *
   * @return array
   */
  public function index(): array {
    return [
      '#theme' => 'admin_theme_layout',
      '#table' => $this->buildNodeTypeTable(),
    ];
  }

  /**
   * Layout controller access.
   *
   * @param AccountProxy $account
   *
   * @return AccessResultAllowed|AccessResultForbidden
   */
  public function access(AccountProxy $account): AccessResultForbidden|AccessResultAllowed {
    if (!$account->hasPermission('administer node display')) {
      return AccessResult::forbidden();
    }

    if ($account->hasPermission('configure any layout')) {
      return AccessResult::allowed();
    }

    $node_type = array_keys($this->nodeTypes);
    foreach ($node_type as $type) {
      $perm = sprintf(Settings::PERM_LAYOUT_BUILDER_NODE_TYPE_ACCESS, $type);
      if ($account->hasPermission($perm)) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * Build a renderable node type list.
   *
   * @return array
   */
  private function buildNodeTypeTable(): array {
    $weight = 0;
    $node_types = $this->buildNodeTypeTableInfo();
    $view_modes = $node_types['#view_modes'];
    unset($node_types['#view_modes']);

    // Header
    $build = [
      '#type'    => 'table',
      '#caption' => $this->t('Content types'),
      '#header'  => array_merge([
        $this->t('Name'),
        $this->t('Description'),
      ], array_map(function ($mode) {
        return t(ucfirst($mode));
      }, $view_modes)),
    ];

    // Global layout builder info
    $build['global']['#weight'] = $weight;
    $build['global']['name'] = [
      '#markup' => t('Global layout'),
    ];
    $build['global']['description'] = [
      '#markup' => t('Default layout this will be use for all layouts as default.'),
    ];

    $accept = ['default', 'teaser'];
    foreach ($view_modes as $view_mode) {
      $link = [];
      if (in_array($view_mode, $accept)) {
        $url = Url::fromRoute(
          'layout_builder.defaults.node.view', [
          'node_type'      => Settings::THEME_LAYOUT_NODE_TYPE,
          'view_mode_name' => $view_mode,
        ]);
        $url->setOption('query', [
          'destination' => $this->destinationUrl->toString()
        ]);
        $title = $this->getEditLinkTitle($view_mode);
        $link = Link::fromTextAndUrl($title, $url)
          ->toRenderable();
      }
      $build['global'][$view_mode] = $link;
    }

    // Content type list
    $account = Drupal::currentUser();
    asort($node_types);
    foreach ($node_types as $key => $type) {
      $perm = sprintf(Settings::PERM_LAYOUT_BUILDER_NODE_TYPE_ACCESS, $key);
      if ($account->hasPermission($perm)) {
        $build[$key]['#weight'] = $weight;
        $build[$key]['name'] = [
          '#markup' => $type['label'],
        ];
        $build[$key]['description'] = [
          '#markup' => $type['description'],
        ];

        foreach ($view_modes as $view_mode) {
          $link = ['#markup' => $this->t('No exists')];
          if (!empty($type['view_mode'][$view_mode])) {
            $mode = $type['view_mode'][$view_mode];
            /** @var Url $url */
            $url = $mode['url'];
            $url->setOption('query', [
              'destination' => $this->destinationUrl->toString()
            ]);
            $title = $this->getEditLinkTitle($mode['mode']);
            $link = Link::fromTextAndUrl($title, $url)
              ->toRenderable();
          }
          $build[$key][$view_mode] = $link;
        }

        $weight++;
      }
    }

    return $build;
  }

  /**
   * Build a renderable node type list.
   *
   * @return array
   */
  private function buildNodeTypeTableInfo(): array {
    $ret = [];
    $view_modes_list = [];
    foreach ($this->nodeTypes as $key => $nodeType) {
      $view_modes = $this->entityViewDisplay
        ->loadByProperties([
          'targetEntityType' => 'node',
          'bundle'           => $key,
        ]);

      /** @noinspection PhpPossiblePolymorphicInvocationInspection */
      $ret[$key] = [
        'label'       => $nodeType->label(),
        'description' => $nodeType->getDescription(),
      ];

      /** @var LayoutBuilderEntityViewDisplay $view_mode */
      foreach ($view_modes as $view_mode) {
        $mode = $view_mode->getMode();
        $url = Url::fromRoute(
          'layout_builder.defaults.node.view', [
          'node_type'      => $key,
          'view_mode_name' => $mode,
        ]);
        $ret[$key]['view_mode'][$mode]['mode'] = $mode;
        $ret[$key]['view_mode'][$mode]['url'] = $url;
        if ($mode != 'default') {
          $view_modes_list[] = $mode;
        }
      }
    }

    $view_modes_list = array_unique($view_modes_list);
    array_unshift($view_modes_list, 'default');
    $ret['#view_modes'] = $view_modes_list;
    return $ret;
  }

  /**
   * Get edit link translatable markup.
   *
   * @param string $mode
   *
   * @return TranslatableMarkup
   */
  private function getEditLinkTitle(string $mode): TranslatableMarkup {
    return $this->t('Edit @label', [
      '@label' => $mode,
    ]);
  }

}
