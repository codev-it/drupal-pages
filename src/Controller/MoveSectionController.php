<?php

namespace Drupal\codev_pages\Controller;

use Drupal\codev_pages\NestedSectionManger;
use Drupal\codev_pages\Settings;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to move a block.
 *
 * @noinspection PhpUnused
 */
class MoveSectionController implements ContainerInjectionInterface {

  use LayoutRebuildTrait;

  /**
   * The layout temp store repository.
   *
   * @var LayoutTempstoreRepositoryInterface
   */
  protected LayoutTempstoreRepositoryInterface $layoutTempstoreRepository;

  /**
   * Third party setting provider.
   *
   * @var string
   */
  private string $provider = Settings::SECTION_THIRD_PARTY_SETTING_PROVIDER;

  /**
   * LayoutController constructor.
   *
   * @param LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore repository.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): MoveSectionController {
    /** @noinspection PhpParamsInspection */
    return new static($container->get('layout_builder.tempstore_repository'));
  }

  /**
   * Moves a block to another region.
   *
   * @param SectionStorageInterface $section_storage
   *   The section storage.
   * @param int                                            $delta_from
   *   The delta of the original section.
   * @param int                                            $delta_to
   *   The delta of the destination section.
   * @param int|null                                       $parent_to
   * @param string|null                                    $region_to
   *   The new region for this block.
   * @param int|null                                       $weight
   *
   * @return AjaxResponse
   *   An AJAX response.
   */
  public function build(SectionStorageInterface $section_storage, int $delta_from, int $delta_to, ?int $parent_to, ?string $region_to, ?int $weight): AjaxResponse {
    $section = $section_storage->getSection($delta_from);

    $section->setThirdPartySetting($this->provider, 'region', $region_to);
    $section->setThirdPartySetting($this->provider, 'weight', $weight);
    if (isset($parent_to)) {
      $section_uuid = $section->getThirdPartySetting($this->provider, 'uuid');
      $parent_section = $section_storage->getSection($parent_to);
      $parent_uuid = $parent_section->getThirdPartySetting($this->provider, 'uuid');
      $section->setThirdPartySetting($this->provider, 'parent', $parent_uuid);
      $weight_info = NestedSectionManger::buildItemsWeightInfos($parent_to, $section_storage, $section_uuid);
      NestedSectionManger::appendItemsWeightInfos($weight, [
        'type' => 'section',
        'uuid' => $section_uuid,
      ], $weight_info);
      NestedSectionManger::updateItemsWeightInfos($weight_info, $parent_section, $section_storage);
    }
    else {
      $section->setThirdPartySetting($this->provider, 'parent', NULL);
    }

    if ($delta_from !== $delta_to) {
      $section_storage->removeSection($delta_from);
      $section_storage->insertSection($delta_to, $section);
    }

    $this->layoutTempstoreRepository->set($section_storage);
    return $this->rebuildLayout($section_storage);
  }

}
