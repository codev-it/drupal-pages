<?php

namespace Drupal\codev_pages;

use Drupal\codev_utils\Helper\Utils;
use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_pages
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public const MODULE_NAME = 'codev_pages';

  /**
   * Theme administer permission key
   */
  public const THEME_LAYOUT_NODE_TYPE = 'layout_builder_global';

  /**
   * Administer all content types permission key
   */
  public const PERM_ADMIN_NODE_TYPES = 'administer content types';

  /**
   * Layout builder content type permission key
   */
  public const PERM_LAYOUT_BUILDER_NODE_TYPE_ACCESS = 'configure all %s node layout overrides';

  /**
   * Third Party Setting provider key
   */
  public const SECTION_THIRD_PARTY_SETTING_PROVIDER = 'layout_builder_nested_section';

  /**
   * Get the grid simply layouts formatted data.
   *
   * @return array
   */
  public static function getGridSimplyLayouts(): array {
    $ret = [];
    foreach (static::get('grid_simply_layouts') as $key => $val) {
      if (is_array($val)) {
        foreach ($val as $val_key => $val_data) {
          if (is_array($val_data)) {
            foreach ($val_data as $val_column) {
              $id = Utils::getArrayValue('id', $val_column, '');
              $column = Utils::getArrayValue('columns', $val_column, []);
              $ret[$key][$val_key][$id] = $column;
            }
          }
        }
      }
    }
    return $ret;
  }

}
