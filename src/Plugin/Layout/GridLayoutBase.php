<?php

namespace Drupal\codev_pages\Plugin\Layout;

use Drupal;
use Drupal\codev_pages\Plugin\LayoutSettingsPluginManager;
use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Layout\LayoutDefinition;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: GridLayout.php
 * .
 */

/**
 * Grid layout section, base class.
 */
abstract class GridLayoutBase extends LayoutDefault {

  /**
   * Layout setting object.
   *
   * @var LayoutSettingsPluginManager
   */
  protected LayoutSettingsPluginManager $layoutSettings;

  /**
   * Settings array.
   *
   * @var array
   */
  protected array $settings = [];

  /**
   * Settings parents array.
   *
   * @var array
   */
  protected array $pluginSettingsParentKeys = ['plugins'];

  /**
   * Default max grid size.
   *
   * @var int
   */
  protected mixed $defGridMaxSize;

  /**
   * Grid select size field name.
   */
  protected string $gridSelectFieldName = '';

  /**
   * {@inheritdoc}
   *
   * @throws PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $settings_key = $this->pluginSettingsParentKeys;
    $this->settings = Settings::toArray();
    $this->defGridMaxSize = $this->settings['grid_size'];
    $this->layoutSettings = Drupal::service('plugin.manager.layout_settings');
    $this->layoutSettings->loadApplication($this, $this->getConfiguration()[$settings_key[0]]);
    $this->layoutSettings->setParents($settings_key);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $configuration = parent::defaultConfiguration();
    $configuration += [
      'container'                        => [],
      'row'                              => [],
      'columns'                          => [],
      $this->pluginSettingsParentKeys[0] => [],
    ];
    return $configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $regions): array {
    $regions = parent::build($regions);
    $config = $this->getConfiguration();
    $columns = $config['columns'] ?: [];
    $container_attr = $this->layoutSettings->buildContainerAttributes($config);
    $row_attr = $this->layoutSettings->buildRowAttributes($config);
    $container = Utils::getArrayValue('container', $config, []);
    $row = Utils::getArrayValue('row', $config, []);
    $regions['#container_attributes'] = Utils::mergeAttributesArray($container_attr, $container);
    $regions['#attributes'] = Utils::mergeAttributesArray($row_attr, $row);

    if (!empty($regions['#layout'])) {
      /** @var LayoutDefinition $layout */
      $layout = $regions['#layout'];
      $layout_regions = $layout->getRegions();
      foreach (array_keys($layout_regions) as $i => $region) {
        if ($i >= count($columns)) {
          $regions[$region]['#render'] = FALSE;
        }
        else {
          $attributes = !empty($regions[$region]['#attributes']) ? $regions[$region]['#attributes'] : [];
          $column = Utils::getArrayValue($region, $columns, []);
          $regions[$region]['#weight'] = $i;
          $regions[$region]['#render'] = TRUE;
          $regions[$region]['#attributes'] = Utils::mergeAttributesArray(
            $attributes, $column,
            $this->layoutSettings->buildColumnsAttributes(
              $region, $config, $i + 1, count($columns)
            )
          );
        }
      }
    }
    return $regions;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form_state->set('grid_select_field_name', $this->gridSelectFieldName);
    return $this->layoutSettings->appendWidgets($this->getConfiguration(), $form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);
    $this->layoutSettings->validateWidgets($form, $form_state);
  }

  /**
   * {@inheritDoc}
   *
   * @throws PluginException
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $settings_key = $this->pluginSettingsParentKeys;
    $this->layoutSettings->loadApplication($this, $this->getConfiguration()[$settings_key[0]]);
    $this->layoutSettings->setParents($settings_key);
    $this->configuration[$settings_key[0]] = $this->layoutSettings->saveWidgets($form_state);
  }

  /**
   * Filter empty value from array.
   *
   * @param array $arr
   *
   * @return array
   */
  protected function filterEmptyColumns(array $arr): array {
    foreach ($arr as $key => $item) {
      if (!empty($item) && is_array($item)) {
        foreach ($item as $k_item => $v_item) {
          if (empty($v_item)) {
            unset($arr[$key][$k_item]);
          }
        }
        if (empty($arr[$key])) {
          unset($arr[$key]);
        }
      }
    }
    return $arr;
  }

}
