<?php

namespace Drupal\codev_pages\Plugin\Layout;

use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: GridLayout.php
 * .
 */

/**
 * Simply grid layout section, for simply content editing.
 */
class GridLayoutSimply extends GridLayoutBase {

  /**
   * Default grid container class.
   *
   * @var string
   */
  protected mixed $gridContainerClass;

  /**
   * Default grid gutter class.
   *
   * @var string
   */
  protected mixed $gridGutterClass;

  /**
   * Default grid row class.
   *
   * @var string
   */
  protected mixed $gridRowClass;

  /**
   * Default grid column class.
   *
   * @var string
   */
  protected mixed $gridColClass;

  /**
   * Device css column class prefixes.
   *
   * @var array
   */
  protected mixed $gridColPrefix;

  /**
   * Grid simply layouts presets
   *
   * @var array
   */
  private array $gridSimplyLayouts;

  /**
   * Grid select size field name.
   */
  protected string $gridSelectFieldName = '[grid][select]';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->gridContainerClass = $this->settings['grid_container_class'];
    $this->gridGutterClass = $this->settings['grid_gutter_class'];
    $this->gridRowClass = $this->settings['grid_row_class'];
    $this->gridColClass = $this->settings['grid_col_class'];
    $this->gridColPrefix = $this->settings['grid_col_prefix'];
    $this->gridSimplyLayouts = Settings::getGridSimplyLayouts();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $configuration = parent::defaultConfiguration();
    $configuration['gutter_enable'] = TRUE;
    $configuration['columns_default'] = '';
    return $configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['#attached']['library'][] = 'codev_pages/layout-builder-section-paragraphs';

    $form['grid'] = [
      '#type'       => 'container',
      '#weight'     => -99999,
      '#attributes' => [
        'class' => [
          'layout-builder-select-header',
        ],
      ],
    ];

    $form['grid']['container'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Grid container'),
      '#default_value' => !empty($config['container']['class'])
        && in_array($this->gridContainerClass, $config['container']['class']),
    ];

    $form['grid']['gutter'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Grid gutter'),
      '#default_value' => $this->configuration['gutter_enable'],
    ];

    $form['grid']['select'] = [
      '#type'          => 'radios',
      '#title'         => t('Columns count:'),
      '#options'       => $this->buildGridSelectOptions(),
      '#default_value' => !empty($config['columns'])
        ? count($config['columns']) : 1,
      '#required'      => TRUE,
      '#attributes'    => [
        'class' => [
          'layout-builder-select-grid-size',
        ],
      ],
    ];

    $form['grid']['columns'] = [
      '#type'       => 'fieldset',
      '#attributes' => [
        'class' => [
          'layout-builder-select-grid-layout',
        ],
      ],
    ];

    foreach ($this->gridSimplyLayouts as $key => $layout) {
      $this->buildColumnLayout($form['grid']['columns'], $key, $layout);
    }

    $align_default = '';
    if (!empty($config['row']['class'])) {
      foreach ($config['row']['class'] as $class) {
        if (in_array($class, $this->getGridAlignOptions())) {
          $align_default = $class;
        }
      }
    }
    $form['grid']['align'] = [
      '#type'          => 'radios',
      '#title'         => t('Alignment:'),
      '#options'       => array_combine(
        $this->getGridAlignOptions(), $this->getGridAlignOptions()
      ),
      '#default_value' => $align_default,
      '#attributes'    => [
        'class' => [
          'layout-builder-select-grid-align',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $grid = $form_state->getValue('grid');
    $container = [];
    $row = ['class' => [$this->gridRowClass]];
    $columns = [];
    $columns_default = '';
    if (!empty($grid['container'])) {
      $container['class'][] = $this->gridContainerClass;
    }

    if (!empty($grid['gutter'])) {
      $row['class'][] = $this->gridGutterClass;
      $this->configuration['gutter_enable'] = TRUE;
    }
    else {
      $this->configuration['gutter_enable'] = FALSE;
    }

    if (!empty($grid['align'])) {
      $row['class'][] = $grid['align'];
    }

    foreach (array_filter($grid['columns']) as $column) {
      $columns_default = $column;
      $column = $this->buildColumns($column);
      foreach ($column as $key => $value) {
        $id = sprintf('col_%s', $key + 1);
        $columns[$id] = $this->filterEmptyColumns(
          $this->buildColumnsAttributes($value ?: []));
      }
    }

    $this->configuration['container'] = $container;
    $this->configuration['row'] = $row;
    $this->configuration['columns'] = $columns;
    $this->configuration['columns_default'] = $columns_default;
  }

  /**
   * Return an array for the grid select col size.
   */
  private function buildGridSelectOptions(): array {
    $ret = [];
    for ($i = 1; $i <= count($this->gridSimplyLayouts); $i++) {
      $ret[$i] = $i;
    }
    return $ret;
  }

  /**
   * Return an array for the grid align options.
   */
  private function getGridAlignOptions(): array {
    return [
      'align-left',
      'align-center',
      'align-right',
      'align-left-middle',
      'align-center-middle',
      'align-right-middle',
      'align-left-bottom',
      'align-center-bottom',
      'align-right-bottom',
    ];
  }

  /**
   * Build the columns select list.
   *
   * @param array  $elem
   * @param string $value
   * @param array  $configs
   */
  private function buildColumnLayout(array &$elem, string $value, array $configs): void {
    $options = [];
    foreach ($configs as $config) {
      $key = Json::encode($config);
      $options[$key] = $this->buildLayoutSelectMarkup($config);
    }
    $elem[$value] = [
      '#type'          => 'radios',
      '#title'         => t('Columns layout:'),
      '#options'       => $options,
      '#default_value' => $value == count($this->getConfiguration()['columns'])
        ? $this->getConfiguration()['columns_default'] : '',
      '#states'        => [
        'visible' => [
          sprintf(':input[name*="%s"]', $this->gridSelectFieldName) => [
            'value' => $value,
          ],
        ],
      ],
    ];
  }

  /**
   * Build the grid select markup.
   *
   * @param array $grid
   *
   * @return string
   */
  private function buildLayoutSelectMarkup(array $grid): string {
    $column = [];
    $columns = [];
    $prefix_data = !empty($this->gridColPrefix) ? array_values($this->gridColPrefix)[0] : [];
    $prefix = Utils::getArrayValue('prefix', $prefix_data, '');
    foreach ($grid as $value) {
      if (!empty($value)) {
        $column = $value;
      }
    }
    foreach ($column as $value) {
      $columns[] = sprintf('<div class="item %s %s%s">' .
        '<span class="content"></span>' .
        '</div>', $this->gridColClass, $prefix, $value);
    }
    return sprintf('<div class="cnt %s">%s</div>',
      $this->gridRowClass, implode('', $columns));
  }

  /**
   * Build columns array.
   *
   * @param string $column
   *
   * @return array
   */
  private function buildColumns(string $column): array {
    $ret = [];
    $data = Json::decode($column);
    foreach ($data as $type => $item) {
      foreach ($item as $key => $value) {
        $ret[$key][$type] = $value;
      }
    }
    return $ret;
  }

  /**
   * Build the columns attributes.
   *
   * @param array $column
   *
   * @return array
   */
  private function buildColumnsAttributes(array $column): array {
    $classes = [$this->gridColClass];
    $prefixes = $this->getCleanGridColPrefix();
    foreach ($column ?: [] as $key => $value) {
      $prefix = Utils::getArrayValue($key, $prefixes, '');
      $classes[] = $prefix . $value;
    }
    return ['class' => $classes];
  }

  /**
   * Get the grid col prefixes as simply array.
   */
  private function getCleanGridColPrefix(): array {
    $ret = [];
    foreach ($this->gridColPrefix ?: [] as $val) {
      $ret[$val['id']] = $val['prefix'];
    }
    return $ret;
  }

}
