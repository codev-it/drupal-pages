<?php

namespace Drupal\codev_pages\Plugin\Layout;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: GridLayout.php
 * .
 */

/**
 * Advanced grid layout section, for advanced content editing.
 */
class GridLayoutAdvanced extends GridLayoutBase {

  /**
   * Grid select size field name.
   */
  protected string $gridSelectFieldName = '[grid][columns][select]';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['#attached']['library'][] = 'codev_pages/json-editor';
    $form['#attached']['library'][] = 'codev_pages/layout-builder-section-dialog';
    $form['tab-header'] = [
      '#type'          => 'radios',
      '#default_value' => 'grid',
      '#options'       => [
        'grid'    => $this->t('Grid'),
        'plugins' => $this->t('Plugins'),
      ],
      '#attributes'    => [
        'class' => [
          'layout-builder-tab-icon',
        ],
      ],
      '#prefix'        => Markup::create('<div id="layout-builder-tabs">'),
      '#suffix'        => Markup::create('</div>'),
      '#weight'        => -99999,
    ];

    // Grid container
    $form['grid'] = [
      '#type'   => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name*="[tab-header]"]' => [
            'value' => 'grid',
          ],
        ],
      ],
    ];

    // Settings container
    $form['plugins']['#type'] = 'fieldset';
    $form['plugins']['#states'] = [
      'visible' => [
        ':input[name*="[tab-header]"]' => [
          'value' => 'plugins',
        ],
      ],
    ];

    // Grid main
    $form['grid']['container'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Container attributes'),
      '#default_value' => !empty($config['container'])
        ? Json::encode($config['container']) : '',
      '#attributes'    => [
        'class' => [
          'json-editor',
        ],
      ],
    ];

    $form['grid']['row'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Row attributes'),
      '#default_value' => !empty($config['row']) ? Json::encode($config['row']) : '',
      '#attributes'    => [
        'class' => [
          'json-editor',
        ],
      ],
    ];

    $form['grid']['columns'] = [
      '#type'   => 'container',
      '#prefix' => Markup::create('<div id="grid-select">'),
      '#suffix' => Markup::create('</div>'),
    ];

    $columns_count_def = !empty($config['columns']) ? count($config['columns']) : 1;
    $columns_count = $form_state->get('columns_count') ?: $columns_count_def;
    $form_state->set('columns_count', $columns_count);
    $form['grid']['columns']['select'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Section column size:'),
      '#options'       => $this->buildGridSelectOptions(),
      '#default_value' => $columns_count,
      '#required'      => TRUE,
      '#ajax'          => [
        'callback'        => [$this, 'gridSelectColumnsAjaxCallback'],
        'disable-refocus' => FALSE,
        'event'           => 'change',
        'wrapper'         => 'grid-select',
      ],
    ];

    $form['grid']['columns']['items'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'layout-settings-grid-main-columns',
        ],
      ],
    ];

    $this->builderGridColumnsFormItems($columns_count,
      $form['grid']['columns']['items'], $config['columns']);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $grid = $form_state->getValue('grid');
    $row = !empty($grid['row']) ? Json::decode($grid['row']) : [];
    $container = !empty($grid['container']) ? Json::decode($grid['container']) : [];
    $columns = !empty($grid['columns']['items']) ?
      $this->filterEmptyColumns($grid['columns']['items']) : [];
    $this->configuration['container'] = $this->filterEmptyColumns($container);
    $this->configuration['row'] = $this->filterEmptyColumns($row);
    $this->configuration['columns'] = array_map(function ($column) {
      return Json::decode($column['attributes']);
    }, $columns);
  }

  /**
   * Grid select list grid info update.
   *
   * @param array                                $form
   * @param FormStateInterface $form_state
   *
   * @return mixed
   */
  public function gridSelectColumnsAjaxCallback(array &$form, FormStateInterface $form_state): mixed {
    $layout_settings = $form_state->getValue('layout_settings');
    $selected = !empty($layout_settings['grid']['columns']['select'])
      ? intval($layout_settings['grid']['columns']['select']) : 1;
    $this->builderGridColumnsFormItems($selected,
      $form['layout_settings']['grid']['columns']['items']);
    return $form['layout_settings']['grid']['columns'];
  }

  /**
   * Return an array for the grid select col size.
   */
  private function buildGridSelectOptions(): array {
    $ret = [];
    for ($i = 1; $i <= $this->defGridMaxSize; $i++) {
      $ret[$i] = $i;
    }
    return $ret;
  }

  /**
   * Build a renderable grid columns form fields.
   *
   * @param int   $count
   * @param array $def
   * @param array $elem
   */
  private function builderGridColumnsFormItems(int $count, array &$elem, array $def = []): void {
    if ($count > 0 && $count <= $this->defGridMaxSize) {
      $states = [];
      for ($i = 0; $i < $this->defGridMaxSize; $i++) {
        $id = sprintf('col_%s', $i + 1);
        if (empty($elem[$id]['#type'])) {
          $elem[$id]['#type'] = 'fieldset';
        }
        if (empty($elem[$id]['#title'])) {
          $elem[$id]['#title'] = $this->t('Column @key', [
            '@key' => $i + 1,
          ]);
        }
        if (empty($elem[$id]['attributes'])) {
          $elem[$id]['attributes'] = [
            '#type'          => 'textfield',
            '#default_value' => !empty($def[$id]) ? Json::encode($def[$id]) : '',
            '#attributes'    => [
              'class' => [
                'json-editor',
              ],
            ],
          ];
        }
        if (empty($elem[$id]['#states'])) {
          $states[] = ['value' => $i];
          $elem[$id]['#states'] = [
            'invisible' => [
              sprintf(':input[name*="%s"]', $this->gridSelectFieldName) => $states,
            ],
          ];
        }
      }
    }
  }

}
