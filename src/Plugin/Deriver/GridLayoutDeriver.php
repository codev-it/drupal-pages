<?php

namespace Drupal\codev_pages\Plugin\Deriver;

use Drupal\codev_pages\Plugin\Layout\GridLayoutAdvanced;
use Drupal\codev_pages\Plugin\Layout\GridLayoutSimply;
use Drupal\codev_pages\Settings;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Layout\LayoutDefinition;

/**
 * Makes a flexible layout for each layout config entity.
 *
 * @noinspection PhpUnused
 */
class GridLayoutDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    /** @noinspection PhpInvalidInstanceofInspection */
    if (!$base_plugin_definition instanceof LayoutDefinition) {
      return $this->derivatives;
    }

    $this->derivatives['simply'] = new LayoutDefinition([
      'class'      => GridLayoutSimply::class,
      'id'         => $base_plugin_definition->id() . '_simply',
      'label'      => t('Grid section simply'),
      'category'   => $base_plugin_definition->getCategory(),
      'regions'    => $this->buildRegionsInfos(),
      'template'   => $base_plugin_definition->getTemplate(),
      'provider'   => $base_plugin_definition->getProvider(),
      'theme_hook' => $base_plugin_definition->getThemeHook(),
      'icon_map'   => $this->defGridIconMapping(),
    ]);

    $this->derivatives['advanced'] = new LayoutDefinition([
      'class'      => GridLayoutAdvanced::class,
      'id'         => $base_plugin_definition->id() . '_advanced',
      'label'      => t('Grid section advanced'),
      'category'   => $base_plugin_definition->getCategory(),
      'regions'    => $this->buildRegionsInfos(),
      'template'   => $base_plugin_definition->getTemplate(),
      'provider'   => $base_plugin_definition->getProvider(),
      'theme_hook' => $base_plugin_definition->getThemeHook(),
      'icon_map'   => $this->defGridIconMapping(),
    ]);

    return $this->derivatives;
  }

  /**
   * Build the region info for the current layout by setting grid size.
   *
   * @return array
   */
  private function buildRegionsInfos(): array {
    $ret = [];
    $grid_size = Settings::get('grid_size');
    for ($i = 1; $i <= $grid_size; $i++) {
      $ret['col_' . $i] = [
        'label' => t('Column @key', [
          '@key' => $i,
        ]),
      ];
    }
    return $ret;
  }

  /**
   * Default grid icon mapping.
   *
   * @return array
   */
  private function defGridIconMapping(): array {
    return [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ];
  }

}
