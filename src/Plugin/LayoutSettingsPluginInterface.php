<?php

namespace Drupal\codev_pages\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: LayoutSettingsPluginInterface.php
 * .
 */

/**
 * Interface LayoutSettingsPluginInterface
 *
 * @package      Drupal\codev_pages
 *
 * @noinspection PhpUnused
 */
interface LayoutSettingsPluginInterface {

  /**
   * Provide id of the layout settings.
   *
   * @return string
   */
  public function id(): string;

  /**
   * Provide a label of the layout settings.
   *
   * @return string|TranslatableMarkup
   *   A string description of the  layout settings.
   */
  public function label(): string|TranslatableMarkup;

  /**
   * Provide a description of the layout settings.
   *
   * @return string|TranslatableMarkup
   *   A string description of the  layout settings.
   */
  public function description(): string|TranslatableMarkup;

  /**
   * Provide a priority of the layout settings widget.
   *
   * @return int
   */
  public function priority(): int;

  /**
   * Provide a weight of the layout settings widget.
   *
   * @return int
   */
  public function weight(): int;

  /**
   * Build the container attributes class.
   *
   * @param array $config
   *
   * @return array
   */
  public function containerAttributes(array $config): array;

  /**
   * Build the row attributes class.
   *
   * @param array $config
   *
   * @return array
   */
  public function rowAttributes(array $config): array;

  /**
   * Build the columns attributes class.
   *
   * @param string $region
   * @param array  $config
   * @param int    $count
   * @param int    $max_columns
   *
   * @return array
   */
  public function columnsAttributes(string $region, array $config, int $count, int $max_columns): array;

  /**
   * Form widget.
   *
   * @param array                                $config
   * @param array                                $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function widget(array $config, array $form, FormStateInterface $form_state): array;

  /**
   * Form validations handler.
   *
   * @param array                                $values
   * @param array                                $form
   * @param FormStateInterface $form_state
   */
  public function validate(array $values, array $form, FormStateInterface $form_state);

  /**
   * Form save.
   *
   * @param array                                $values
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function save(array $values, FormStateInterface $form_state): array;

}
