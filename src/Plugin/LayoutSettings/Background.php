<?php

namespace Drupal\codev_pages\Plugin\LayoutSettings;

use Drupal;
use Drupal\codev_pages\Annotation\LayoutSettings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Background.php
 * .
 */

/**
 * Class Background.
 *
 * @package      Drupal\codev_pages
 *
 * @LayoutSettings(
 *   id = "background",
 *   label = @Translation("Background"),
 *   description = @Translation("Section background image or color."),
 *   append = {
 *     "Drupal\codev_pages\Plugin\Layout\GridLayoutSimply",
 *   },
 *   weight = 1
 * )
 *
 * @noinspection PhpUnused
 */
class Background extends BackgroundImage {

  /**
   * {@inheritdoc}
   */
  public function containerAttributes(array $config): array {
    $type = Utils::getArrayValue('type', $this->configuration, '');
    switch ($type) {
      case 'color':
        if ($this->configuration['color']) {
          return [
            'style' => ['background-color: ' . $this->configuration['color']],
          ];
        }
        else {
          return [];
        }
      case 'image':
        return $this->buildBackgroundAttributes(Utils::getArrayValue('image', $this->configuration));
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function rowAttributes(array $config): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function columnsAttributes(string $region, array $config, int $count, int $max_columns): array {
    return [];
  }

  public function widget(array $config, array $form, FormStateInterface $form_state): array {
    return parent::buildWidgetDetailsWrp() + [
        'background_type' => [
          '#type'          => 'select',
          '#title'         => t('Background type:'),
          '#options'       => [
            'none'  => t('None'),
            'color' => t('Color'),
            'image' => t('Image'),
          ],
          '#default_value' => Utils::getArrayValue('type', $this->configuration, 'none'),
        ],
        'color'           => [
          '#type'          => 'color',
          '#default_value' => Utils::getArrayValue('color', $this->configuration, ''),
          '#states'        => [
            'visible' => [
              ':input[name*="[background_type]"]' => [
                'value' => 'color',
              ],
            ],
          ],
        ],
        'image'           => [
          '#type'   => 'container',
          '#states' => [
            'visible' => [
              ':input[name*="[background_type]"]' => [
                'value' => 'image',
              ],
            ],
          ],
          'target'  => [
            '#type'              => 'managed_file',
            '#upload_location'   => $this->uploadLocation,
            '#multiple'          => FALSE,
            '#default_value'     => Utils::getArrayValue('image', $this->configuration, []),
            '#upload_validators' => [
              'file_validate_extensions' => $this->fileValidateExtensions,
            ],
          ],
        ],
      ];
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnhandledExceptionInspection
   */
  public function save(array $values, FormStateInterface $form_state): array {
    $image = !empty($values['image']['target'])
      ? end($values['image']['target']) : NULL;
    $file = !empty($image) ? Drupal::entityTypeManager()
      ->getStorage('file')
      ->load($image) : $image;
    if (!empty($file)) {
      $file_usage = Drupal::service('file.usage');
      $file_usage->delete($file, 'codev_pages', 'file', $file->id());
      $file_usage->add($file, 'codev_pages', 'file', $file->id());
    }
    return [
      'type'  => Utils::getArrayValue('background_type', $values),
      'color' => Utils::getArrayValue('color', $values),
      'image' => $image,
    ];
  }

}
