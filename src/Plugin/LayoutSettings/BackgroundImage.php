<?php

namespace Drupal\codev_pages\Plugin\LayoutSettings;

use Drupal;
use Drupal\codev_pages\Annotation\LayoutSettings;
use Drupal\codev_pages\Plugin\LayoutSettingsPluginBase;
use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Media;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: BackgroundImage.php
 * .
 */

/**
 * Class BackgroundImage.
 *
 * @package      Drupal\codev_pages
 *
 * @LayoutSettings(
 *   id = "background_image",
 *   label = @Translation("Background image"),
 *   description = @Translation("Section background image."),
 *   append = {
 *     "Drupal\codev_pages\Plugin\Layout\GridLayoutAdvanced",
 *   },
 *   weight = 1
 * )
 *
 * @noinspection PhpUnused
 */
class BackgroundImage extends LayoutSettingsPluginBase {

  /**
   * Background image upload image.
   *
   * @var string
   */
  protected string $uploadLocation = 'public://background-images/';

  /**
   * file_validate_extensions.
   *
   * @var array
   */
  protected array $fileValidateExtensions = ['png gif jpg jpeg svg'];

  /**
   * {@inheritdoc}
   *
   */
  public function containerAttributes(array $config): array {
    return $this->buildBackgroundAttributes(Utils::getArrayValue('container', $this->configuration));
  }

  /**
   * {@inheritdoc}
   *
   */
  public function rowAttributes(array $config): array {
    return $this->buildBackgroundAttributes(Utils::getArrayValue('row', $this->configuration));
  }

  /**
   * {@inheritdoc}
   *
   */
  public function columnsAttributes(string $region, array $config, int $count, int $max_columns): array {
    return $this->buildBackgroundAttributes(
      !empty($this->configuration['columns'][$count])
        ? $this->configuration['columns'][$count] : NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function widget(array $config, array $form, FormStateInterface $form_state): array {
    $columns = [];
    $states = [];
    $field_name = $form_state->get('grid_select_field_name');
    for ($i = 1; $i <= Settings::get('grid_size'); $i++) {
      $states[] = ['value' => $i - 1];
      $columns[$i] = [
        '#type'   => 'container',
        '#states' => [
          'invisible' => [
            sprintf(':input[name*="%s"]', $field_name) => $states,
          ],
        ],
        'image'   => [
          '#type'              => 'managed_file',
          '#title'             => t('Column @key background image:', [
            '@key' => $i,
          ]),
          '#upload_location'   => $this->uploadLocation,
          '#multiple'          => FALSE,
          '#default_value'     => !empty($this->configuration['columns'][$i]) ?
            [$this->configuration['columns'][$i]] : [],
          '#upload_validators' => [
            'file_validate_extensions' => $this->fileValidateExtensions,
          ],
        ],
      ];
    }

    return parent::widget($config, $form, $form_state) + [
        'container' => [
          '#type'              => 'managed_file',
          '#title'             => t('Container background image:'),
          '#upload_location'   => $this->uploadLocation,
          '#multiple'          => FALSE,
          '#default_value'     => [Utils::getArrayValue('container', $this->configuration, '')],
          '#upload_validators' => [
            'file_validate_extensions' => $this->fileValidateExtensions,
          ],
        ],
        'row'       => [
          '#type'              => 'managed_file',
          '#title'             => t('Row background image:'),
          '#upload_location'   => $this->uploadLocation,
          '#multiple'          => FALSE,
          '#default_value'     => [Utils::getArrayValue('row', $this->configuration, '')],
          '#upload_validators' => [
            'file_validate_extensions' => $this->fileValidateExtensions,
          ],
        ],
        'columns'   => $columns,
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $values, FormStateInterface $form_state): array {
    $container = !empty($values['container']) ? end($values['container']) : NULL;
    $row = !empty($values['row']) ? end($values['row']) : NULL;
    $columns = [];

    if (!empty($this->configuration['columns'])) {
      foreach ($this->configuration['columns'] as $column) {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->removeFileUsage($column);
      }
    }

    if (!empty($values['columns'])) {
      foreach ($values['columns'] as $key => $column) {
        $image = end($column['image']);
        $usage = $this->setFileUsage($image ?: NULL);
        if (!empty($usage)) {
          $columns[$key] = $usage;
        }
      }
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->removeFileUsage($container);
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->removeFileUsage($row);
    return [
      'container' => $this->setFileUsage($container),
      'row'       => $this->setFileUsage($row),
      'columns'   => $columns,
    ];
  }

  /**
   * Build the default attributes by given file id.
   *
   * @param int|null $fid
   *
   * @return array
   */
  protected function buildBackgroundAttributes(?int $fid): array {
    /** @var File $file */
    $file_url = Media::getUrlByFileId($fid, 'background_default');
    if (!empty($file_url)) {
      return [
        'class' => [
          'background-image',
        ],
        'style' => [
          sprintf('background-image: url(%s);', $file_url),
        ],
      ];
    }
    else {
      return [];
    }
  }

  /**
   * Set background file usage.
   *
   * @param int|null $fid
   *
   * @return int|null
   */
  private function setFileUsage(?int $fid): ?int {
    try {
      if (!empty($fid)) {
        $file = Drupal::entityTypeManager()
          ->getStorage('file')
          ->load($fid);
        if (!empty($file)) {
          Drupal::service('file.usage')
            ->add($file, 'codev_pages', 'file', $file->id());
        }
      }
      return $fid;
    } catch (Exception) {
      return NULL;
    }
  }

  /**
   * Remove background file usage.
   *
   * @param int|null $fid
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  private function removeFileUsage(?int $fid): void {
    if (!empty($fid)) {
      $file = Drupal::entityTypeManager()
        ->getStorage('file')
        ->load($fid);
      if (!empty($file)) {
        Drupal::service('file.usage')
          ->delete($file, 'codev_pages', 'file', $file->id());
      }
    }
  }

}
