<?php

namespace Drupal\codev_pages\Plugin\LayoutSettings;

use Drupal\codev_pages\Annotation\LayoutSettings;
use Drupal\codev_pages\Plugin\LayoutSettingsPluginBase;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: ExtraAttributes.php
 * .
 */

/**
 * Class BackgroundImage.
 *
 * @package      Drupal\codev_pages
 *
 * @LayoutSettings(
 *   id = "extra_attributes",
 *   label = @Translation("Extra attributes"),
 *   description = @Translation("Additional attributes."),
 *   append = {
 *     "Drupal\codev_pages\Plugin\Layout\GridLayoutSimply",
 *   },
 *   weight = -99,
 *   priority = 1000
 * )
 *
 * @noinspection PhpUnused
 */
class ExtraAttributes extends LayoutSettingsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function containerAttributes(array $config): array {
    $attr = [];
    if (!empty($this->configuration['id'])) {
      $attr['id'] = $this->configuration['id'];
    }
    if (!empty($this->configuration['container'])) {
      $attr['class'] = $this->configuration['container'];
    }
    return $attr;
  }

  /**
   * {@inheritdoc}
   */
  public function rowAttributes(array $config): array {
    if (!empty($this->configuration['row'])) {
      return ['class' => $this->configuration['row']];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function columnsAttributes(string $region, array $config, int $count, int $max_columns): array {
    if (!empty($this->configuration['columns'])) {
      return ['class' => $this->configuration['columns']];
    }
    else {
      return [];
    }
  }

  public function widget(array $config, array $form, FormStateInterface $form_state): array {
    return parent::widget($config, $form, $form_state) + [
        'id'        => [
          '#type'          => 'textfield',
          '#title'         => t('Section id:'),
          '#default_value' => Utils::getArrayValue('id', $this->configuration, ''),
        ],
        'container' => [
          '#type'          => 'textfield',
          '#title'         => t('Section classes:'),
          '#description'   => t('Separate multiple classes with spaces or commas.'),
          '#default_value' => Utils::getArrayValue('container', $this->configuration, ''),
        ],
        'row'       => [
          '#type'          => 'textfield',
          '#title'         => t('Row classes:'),
          '#description'   => t('Separate multiple classes with spaces or commas.'),
          '#default_value' => Utils::getArrayValue('row', $this->configuration, ''),
        ],
        'columns'   => [
          '#type'          => 'textfield',
          '#title'         => t('Columns classes:'),
          '#description'   => t('Separate multiple classes with spaces or commas.'),
          '#default_value' => Utils::getArrayValue('columns', $this->configuration, ''),
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $values, FormStateInterface $form_state): array {
    return [
      'id'        => Utils::getArrayValue('id', $values, ''),
      'container' => Utils::strSplitValues($values['container'] ?? ''),
      'row'       => Utils::strSplitValues($values['row'] ?? ''),
      'columns'   => Utils::strSplitValues($values['columns'] ?? ''),
    ];
  }

}
