<?php

namespace Drupal\codev_pages\Plugin;

use Drupal\codev_pages\Annotation\LayoutSettings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

/**
 * A plugin manager for layout settings plugins.
 *
 * @noinspection PhpUnused
 */
class LayoutSettingsPluginManager extends DefaultPluginManager {

  /**
   * Array key parents array.
   *
   * @var array
   */
  private array $parents = [];

  /**
   * Application widgets storage.
   *
   * @var LayoutSettingsPluginInterface[]
   */
  private array $widgets = [];

  /**
   * Creates the discovery object.
   *
   * @param Traversable            $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param CacheBackendInterface  $cache_backend
   *   Cache backend instance to use.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LayoutSettings',
      $namespaces, $module_handler,
      LayoutSettingsPluginInterface::class,
      LayoutSettings::class
    );
    $this->alterInfo('layout_settings_info');
    $this->setCacheBackend($cache_backend, 'layout_settings_plugins');
  }

  /**
   * Load the applications.
   *
   * @param LayoutInterface $layout
   * @param array           $config
   *
   * @throws PluginException
   */
  public function loadApplication(LayoutInterface $layout, array $config): void {
    $this->widgets = [];
    foreach ($this->getDefinitions() ?: [] as $application_id => $application) {
      if (!empty($application['append'])) {
        foreach ($application['append'] as $append) {
          if ($layout instanceof $append) {
            $this->widgets[$application_id] = $this->createInstance(
              $application_id, Utils::getArrayValue($application_id, $config, [])
            );
          }
        }
      }
    }
  }

  /**
   * Return the current row attributes.
   *
   * @param array $config
   *
   * @return array
   */
  public function buildContainerAttributes(array $config): array {
    $ret = [];
    $widgets = $this->getWidgetByPriority();
    foreach ($widgets as $widget) {
      $ret = Utils::mergeAttributesArray($ret, $widget->containerAttributes($config));
    }
    return $ret;
  }

  /**
   * Return the current row attributes.
   *
   * @param array $config
   *
   * @return array
   */
  public function buildRowAttributes(array $config): array {
    $ret = [];
    $widgets = $this->getWidgetByPriority();
    foreach ($widgets as $widget) {
      $ret = Utils::mergeAttributesArray($ret, $widget->rowAttributes($config));
    }
    return $ret;
  }

  /**
   * Return the current columns attributes.
   *
   * @param string $region
   * @param array  $config
   * @param int    $count
   * @param int    $max_columns
   *
   * @return array
   */
  public function buildColumnsAttributes(string $region, array $config, int $count, int $max_columns): array {
    $ret = [];
    $widgets = $this->getWidgetByPriority();
    foreach ($widgets as $widget) {
      $ret = Utils::mergeAttributesArray(
        $ret, $widget->columnsAttributes($region, $config, $count, $max_columns)
      );
    }
    return $ret;
  }

  /**
   * Append the loaded application widget to form.
   *
   * @param array              $config
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function appendWidgets(array $config, array $form, FormStateInterface $form_state): array {
    $widgets = [];
    foreach ($this->widgets as $widget) {
      $widgets[$widget->id()] = $widget->widget($config, $form, $form_state);
    }
    return $this->appendToParent($form, $widgets);
  }

  /**
   * Form validation handler.
   *
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function validateWidgets(array $form, FormStateInterface $form_state): array {
    $validate = [];
    $settings = $this->getValuesByParent($form_state->getValues());
    foreach ($this->widgets as $widget) {
      if (!empty($settings[$widget->id()])) {
        $elem = $this->getValuesByParent($form)[$widget->id()];
        $validate[$widget->id()] = $widget->validate($settings[$widget->id()], $elem, $form_state);
      }
    }
    return $validate;
  }

  /**
   * Form save handler.
   *
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function saveWidgets(FormStateInterface $form_state): array {
    $save = [];
    $settings = $this->getValuesByParent($form_state->getValues());
    foreach ($this->widgets as $widget) {
      if (!empty($settings[$widget->id()])) {
        $save[$widget->id()] = $widget->save($settings[$widget->id()], $form_state);
      }
    }
    return $save;
  }

  /**
   * Get parent array key name.
   *
   * @return array
   */
  public function getParents(): array {
    return $this->parents;
  }

  /**
   * Set parents by sting, keys separated by dots.
   *
   * @param array $parents
   */
  public function setParents(array $parents): void {
    $this->parents = $parents;
  }

  /**
   * Return the form values.
   *
   * @param array $arr
   *
   * @return array
   */
  private function getValuesByParent(array $arr): array {
    if (!empty($this->getParents())) {
      foreach ($this->getParents() as $parent) {
        if (!empty($arr[$parent])) {
          $arr = $arr[$parent];
        }
      }
    }
    return $arr;
  }

  /**
   * Append item to array.
   *
   * @param array $arr
   * @param array $append
   *
   * @return array
   */
  private function appendToParent(array $arr, array $append): array {
    NestedArray::setValue($arr, $this->getParents(), $append);
    return $arr;
  }

  /**
   * Sort widget array by priority.
   *
   * @return LayoutSettingsPluginInterface[]
   */
  private function getWidgetByPriority(): array {
    $widgets = $this->widgets;
    Utils::sortObjectByMethod($widgets, 'priority');
    return $widgets;
  }

}
