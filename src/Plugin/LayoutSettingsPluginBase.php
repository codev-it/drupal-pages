<?php

namespace Drupal\codev_pages\Plugin;

use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class LayoutSettingsPluginBase.
 *
 * A base class to help developers implement their own sandwich plugins.
 *
 * @package      Drupal\codev_pages
 *
 * @noinspection PhpUnused
 */
abstract class LayoutSettingsPluginBase extends PluginBase implements LayoutSettingsPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string|TranslatableMarkup {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description(): string|TranslatableMarkup {
    return $this->pluginDefinition['description'];
  }

  /**
   * Provide a priority of the layout settings widget.
   *
   * @return int
   */
  public function priority(): int {
    return Utils::getArrayValue('priority', $this->pluginDefinition, 0);
  }

  /**
   * {@inheritdoc}
   */
  public function weight(): int {
    return $this->pluginDefinition['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public function containerAttributes(array $config): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function rowAttributes(array $config): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function columnsAttributes(string $region, array $config, int $count, int $max_columns): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function widget(array $config, array $form, FormStateInterface $form_state): array {
    return $this->buildWidgetDetailsWrp();
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $values, array $form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  abstract public function save(array $values, FormStateInterface $form_state): array;

  /**
   * Return the default widget details wrapper elem.
   *
   * @return array
   */
  protected function buildWidgetDetailsWrp(): array {
    return [
      '#type'        => 'details',
      '#title'       => $this->label(),
      '#description' => $this->description(),
      '#weight'      => $this->weight(),
    ];
  }

}
