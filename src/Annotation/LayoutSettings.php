<?php /** @noinspection ALL */
/** @noinspection ALL */
/** @noinspection ALL */
/** @noinspection ALL */
/** @noinspection ALL */

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: LayoutSettings.php
 * .
 */

namespace Drupal\codev_pages\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class LayoutSettings.
 *
 * @package Drupal\codev_pages\Annotation
 *
 * @Annotation
 */
class LayoutSettings extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the layout settings type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the layout settings type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Layout classes the are use this settings plugin.
   *
   * Optional.
   *
   * @var array
   */
  public $append;

  /**
   * An integer to determine the priority of this layout settings type.
   *
   * Optional.
   *
   * @var int
   */
  public $priority = 0;

  /**
   * An integer to determine the weight of this layout settings type.
   *
   * Optional.
   *
   * @var int
   */
  public $weight = NULL;

}
