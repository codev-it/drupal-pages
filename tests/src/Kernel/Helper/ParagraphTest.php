<?php

namespace Drupal\Tests\codev_pages\Kernel;

use Drupal\codev_pages\Helper\Paragraph;
use Drupal\paragraphs\Entity\Paragraph as ParagraphE;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: ParagraphTest.php
 * .
 */

/**
 * Class ParagraphTest.
 *
 * Unit tests for the paragraph helper class.
 *
 * @package      Drupal\Tests\codev_pages\Kernel
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class ParagraphTest extends ParagraphTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_pages',
    'codev_pages_test',
    'field',
    'file',
    'layout_builder',
    'layout_discovery',
    'layout_paragraphs',
    'paragraphs',
    'system',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'paragraphs.paragraphs_type.from_library',
  ];

  /**
   * Test: Paragraph::parseText
   */
  public function testParseText() {
    // No text
    $paragraph = ParagraphE::create(['type' => 'no_text']);
    $this->assertEquals('', Paragraph::parseText($paragraph));

    // Text
    /** @var ParagraphE $paragraph */
    $paragraph = ParagraphE::create([
      'type'           => 'text',
      'field_textarea' => [
        'value'  => 'Text',
        'format' => 'basic_html',
      ],
    ]);
    $this->assertEquals('Text', Paragraph::parseText($paragraph));
  }

  /**
   * Test: Paragraph::parseTextMultiple
   */
  public function testParseTextMultiple() {
    $count = 4;
    $accepts = '';
    $paragraphs = [];
    for ($i = 1; $i <= $count; $i++) {
      $paragraphs[] = ParagraphE::create([
        'type'           => 'text',
        'field_textarea' => [
          'value'  => 'Text ' . $i,
          'format' => 'basic_html',
        ],
      ]);
    }
    for ($i = 1; $i <= $count; $i++) {
      $accepts .= 'Text ' . $i;
    }
    $this->assertEquals($count, count($paragraphs));
    $this->assertEquals($accepts, Paragraph::parseTextMultiple($paragraphs));
  }

}
