<?php

namespace Drupal\Tests\codev_pages\Kernel;

use Drupal;
use Drupal\codev_pages\Config;
use Drupal\codev_pages\Settings;
use Drupal\Core\Config\Config as ConfigBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\KernelTests\KernelTestBase;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: ConfigTest.php
 * .
 */

/**
 * Class ConfigTest.
 *
 * Unit tests for the config utility class.
 *
 * @package      Drupal\Tests\codev_pages\Kernel
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class ConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_pages',
    'codev_pages_test',
    'layout_builder',
    'file',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'paragraphs.paragraphs_type.from_library',
  ];

  /**
   * @var ConfigFactory
   */
  protected mixed $configFactory;

  /**
   * @var EntityTypeManagerInterface
   */
  protected mixed $entityTypeManager;

  /**
   * @var string|null
   */
  protected ?string $filePath;

  /**
   * @var FileSystemInterface
   */
  protected mixed $fileSystem;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('file');
    $this->installConfig(['codev_pages_test']);
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->configFactory = $this->container->get('config.factory');
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->filePath = Settings::modulePath() . '/assets/img';
    $this->fileSystem = Drupal::service('file_system');
  }

  /**
   * Test: Config::getParagraphTypeConfig
   *
   * @throws Exception
   */
  public function testGetParagraphTypeConfig() {
    $accept = $this->getParagraphsConfig();
    $config = Config::getParagraphTypeConfig('from_library');
    $this->assertEquals($accept->getRawData() ?: [], $config->getRawData() ?: []);
  }

  /**
   * Test: Config::isParagraphTypeConfig
   *
   * @throws Exception
   */
  public function testIsParagraphTypeConfig() {
    $this->assertEquals('from_library', Config::isParagraphTypeConfig($this->getParagraphsConfig()));

    $config = $this->configFactory->get('test.settings');
    $this->assertNotTrue(Config::isParagraphTypeConfig($config));
  }

  /**
   * Test: Config::setParagraphTypeIconDef
   *
   * @throws Exception
   */
  public function testSetParagraphTypeIconDef() {
    $test_config_before = $this->getParagraphsConfig()->getRawData() ?: [];
    $image = $this->filePath . '/bookmark.svg';
    $this->assertTrue(Config::setParagraphTypeIconDef('from_library', $image));
    $test_config_after = $this->getParagraphsConfig()->getRawData() ?: [];
    $this->assertNotEquals($test_config_before, $test_config_after);

    $copied_file_uri = 'public://paragraphs_type_icon/bookmark.svg';
    $this->assertFileExists($copied_file_uri);
    /** @noinspection PhpUnhandledExceptionInspection */
    /** @var File|null $copied_files */
    $copied_files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $copied_file_uri]);
    $this->assertEmpty($copied_files);
    $file_path = $this->fileSystem->realpath($copied_file_uri);
    $base64_data = base64_encode(file_get_contents($file_path));
    $test_config_before['icon_default'] = 'data:image/svg+xml;base64,' . $base64_data;
    $this->assertEquals($test_config_before, $test_config_after);
  }

  /**
   * Test: Config::setParagraphTypeIcon
   *
   * @noinspection DuplicatedCode
   *
   * @throws Exception
   */
  public function testSetParagraphTypeIcon() {
    // default icon not exist
    $test_config_before = $this->getParagraphsConfig()->getRawData() ?: [];
    $image = $this->filePath . '/bookmark.svg';
    $this->assertTrue(Config::setParagraphTypeIcon('from_library', $image));
    $test_config_after = $this->getParagraphsConfig()->getRawData() ?: [];
    $this->assertNotEquals($test_config_before, $test_config_after);

    $copied_file_uri = 'public://paragraphs_type_icon/bookmark.svg';
    $this->assertFileExists($copied_file_uri);
    /** @noinspection PhpUnhandledExceptionInspection */
    /** @var File|null $copied_file */
    $copied_files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $copied_file_uri]);
    $copied_file = !empty($copied_files) ? end($copied_files) : NULL;
    $file_uuid = !empty($copied_file) ? $copied_file->uuid() : '';
    $file_path = !empty($copied_file) ? $this->fileSystem
      ->realpath($copied_file->getFileUri()) : '';
    $base64_data = base64_encode(file_get_contents($file_path));
    $test_config_before['icon_uuid'] = $file_uuid;
    $test_config_before['dependencies'] = [
      'content' => ['file:file:' . $file_uuid],
    ];
    $test_config_before['icon_default'] = 'data:image/svg+xml;base64,' . $base64_data;
    $this->assertEquals($test_config_before, $test_config_after);

    // default icon exist
    $test_config_before = $this->getParagraphsConfig()->getRawData() ?: [];
    $image = $this->filePath . '/icons.svg';
    $this->assertTrue(Config::setParagraphTypeIcon('from_library', $image));
    $test_config_after = $this->getParagraphsConfig()->getRawData() ?: [];
    $this->assertNotEquals($test_config_before, $test_config_after);

    $copied_file_uri = 'public://paragraphs_type_icon/icons.svg';
    $this->assertFileExists($copied_file_uri);
    /** @noinspection PhpUnhandledExceptionInspection */
    /** @var File|null $copied_file */
    $copied_files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $copied_file_uri]);
    $copied_file = !empty($copied_files) ? end($copied_files) : NULL;
    $file_uuid = !empty($copied_file) ? $copied_file->uuid() : '';
    $test_config_before['icon_uuid'] = $file_uuid;
    $test_config_before['dependencies'] = [
      'content' => ['file:file:' . $file_uuid],
    ];
    $test_config_before['icon_default'] = 'data:image/svg+xml;base64,' . $base64_data;
    $this->assertEquals($test_config_before, $test_config_after);

    // default icon override
    $test_config_before = $this->getParagraphsConfig()->getRawData() ?: [];
    $image = $this->filePath . '/image.svg';
    $this->assertTrue(Config::setParagraphTypeIcon('from_library', $image, TRUE));
    $test_config_after = $this->getParagraphsConfig()->getRawData() ?: [];
    $this->assertNotEquals($test_config_before, $test_config_after);

    $copied_file_uri = 'public://paragraphs_type_icon/image.svg';
    $this->assertFileExists($copied_file_uri);
    /** @noinspection PhpUnhandledExceptionInspection */
    /** @var File|null $copied_file */
    $copied_files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $copied_file_uri]);
    $copied_file = !empty($copied_files) ? end($copied_files) : NULL;
    $file_uuid = !empty($copied_file) ? $copied_file->uuid() : '';
    $file_path = !empty($copied_file) ? $this->fileSystem
      ->realpath($copied_file->getFileUri()) : '';
    $base64_data = base64_encode(file_get_contents($file_path));
    $test_config_before['icon_uuid'] = $file_uuid;
    $test_config_before['dependencies'] = [
      'content' => ['file:file:' . $file_uuid],
    ];
    $test_config_before['icon_default'] = 'data:image/svg+xml;base64,' . $base64_data;
    $this->assertEquals($test_config_before, $test_config_after);
  }

  /**
   * Return the config array for the test paragraphs_type.from_library config.
   *
   * @return Drupal\Core\Config\Config
   *
   * @throws Exception
   */
  private function getParagraphsConfig(): ConfigBase {
    return $this->configFactory->get('paragraphs.paragraphs_type.from_library');
  }

}
