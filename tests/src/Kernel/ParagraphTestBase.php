<?php

namespace Drupal\Tests\codev_pages\Kernel;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: ParagraphTestBase.php
 * .
 */

/**
 * Class ParagraphTestBase.
 *
 * Base paragraph test entity bundles with fields.
 *
 * @package      Drupal\Tests\codev_pages\Kernel
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
abstract class ParagraphTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   *
   * @throws EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    ParagraphsType::create([
      'id'    => 'no_text',
      'label' => 'No Text',
    ])->save();

    ParagraphsType::create([
      'id'    => 'text',
      'label' => 'Text',
    ])->save();

    FieldStorageConfig::create([
      'field_name'  => 'field_textarea',
      'entity_type' => 'paragraph',
      'type'        => 'text_long',
      'cardinality' => -1,
    ])->save();

    FieldConfig::create([
      'field_name'  => 'field_textarea',
      'entity_type' => 'paragraph',
      'bundle'      => 'text',
      'label'       => 'Textarea',
    ])->save();
  }

}
