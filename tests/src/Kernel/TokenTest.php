<?php

namespace Drupal\Tests\codev_pages\Kernel;

use Drupal\codev_utils\Helper\Content;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\token\Functional\TokenTestTrait;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TokenTest.php
 * .
 */

/**
 * Class TokenTest.
 *
 * Unit tests for the config utility class.
 *
 * @package      Drupal\Tests\codev_pages\Kernel
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class TokenTest extends ParagraphTestBase {

  use TokenTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_pages',
    'entity_reference_revisions',
    'field',
    'file',
    'layout_builder',
    'node',
    'paragraphs',
    'system',
    'text',
    'user',
  ];

  /**
   * @var ConfigFactory|null
   */
  protected ?ConfigFactory $configFactory;

  /**
   * @var EntityTypeManagerInterface|null
   */
  protected ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var string|null
   */
  protected ?string $filePath;

  /**
   * @var FileSystemInterface|null
   */
  protected ?FileSystemInterface $fileSystem;

  /**
   * @var Node|null
   */
  protected ?Node $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installConfig(['system']);

    NodeType::create([
      'type' => 'flex_page',
      'name' => 'Flex page',
    ])->save();

    FieldStorageConfig::create([
      'field_name'  => 'body_summary',
      'entity_type' => 'node',
      'type'        => 'text_long',
      'cardinality' => -1,
    ])->save();

    FieldConfig::create([
      'field_name'  => 'body_summary',
      'entity_type' => 'node',
      'bundle'      => 'flex_page',
      'label'       => 'Summary',
    ])->save();

    FieldStorageConfig::create([
      'field_name'  => 'body_flex',
      'entity_type' => 'node',
      'type'        => 'entity_reference_revisions',
      'cardinality' => -1,
      'settings'    => [
        'target_type' => 'paragraph',
      ],
    ])->save();

    FieldConfig::create([
      'field_name'  => 'body_flex',
      'entity_type' => 'node',
      'bundle'      => 'flex_page',
      'label'       => 'Summary',
    ])->save();

  }

  /**
   * Test token: body_flex_summary
   *
   * @throws EntityStorageException
   */
  public function testTokenBodyFlexSummary() {
    $count = 4;
    $accepts = '';
    $paragraphs = [];
    for ($i = 1; $i <= $count; $i++) {
      $paragraph = Paragraph::create([
        'type'           => 'text',
        'field_textarea' => [
          'value'  => 'Text ' . $i,
          'format' => 'basic_html',
        ],
      ]);
      $paragraph->save();
      $paragraph_empty = Paragraph::create(['type' => 'no_text']);
      $paragraph_empty->save();
      $paragraphs[] = [
        'target_id'          => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
      $paragraphs[] = [
        'target_id'          => $paragraph_empty->id(),
        'target_revision_id' => $paragraph_empty->getRevisionId(),
      ];
    }

    for ($i = 1; $i <= $count; $i++) {
      $accepts .= 'Text ' . $i;
    }

    $node = Node::create([
      'type'      => 'flex_page',
      'title'     => 'Test',
      'body_flex' => $paragraphs,
    ]);
    $node->save();

    // Check without setting summary
    $this->assertTokens('codev_pages', ['node' => $node], [
      'body_flex_summary' => $accepts,
    ]);

    // Check without setting summary max length
    $length = 2;
    $this->assertTokens('codev_pages', ['node' => $node], [
      'body_flex_summary:' . $length => Content::subStrByWords($accepts, $length),
    ]);

    // Check with setting summary
    $node->set('body_summary', 'Summary');
    $this->assertTokens('codev_pages', ['node' => $node], [
      'body_flex_summary' => 'Summary',
    ]);
  }

}
