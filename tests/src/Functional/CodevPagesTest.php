<?php

/** @noinspection PhpUnused */

namespace Drupal\Tests\codev_pages\Functional;

use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevPagesTest.php
 * .
 */

/**
 * Class CodevPagesTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_pages\Functional
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class CodevPagesTest extends FunctionalTestBase {

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer pages',
    ]);
  }

  /**
   * Tests the functionality.
   *
   * @throws ExpectationException
   * @throws ResponseTextException
   */
  public function testCodevPages() {
    // Check admin form permission.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/codev_pages');
    $this->assertSession()->statusCodeEquals(200);

    // Check admin form and field exist.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin/config/content/codev_pages');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Select max grid size'));
    $this->submitForm([], t('Save configuration'));

    $this->drupalGet('admin/structure/paragraphs_type');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('From library'));
    $this->assertSession()->pageTextContains(t('Content teaser'));
    $this->assertSession()->pageTextContains(t('Icon'));
    $this->assertSession()->pageTextContains(t('Image'));
    $this->assertSession()->pageTextContains(t('Section'));
    $this->assertSession()->pageTextContains(t('Textarea'));
    $this->assertSession()->pageTextContains(t('View'));

    // Node overview
    $this->drupalGet('node/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists(t('Front page'));
    $this->assertSession()->linkExists(t('Basic page'));
    $this->assertSession()->linkExists(t('Flex page'));

    // Node front page
    $this->drupalGet('node/add/front_page');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Title'));
    $this->assertSession()->pageTextContains(t('Body'));
    $this->drupalCreateNode(['type' => 'front_page']);

    // Node page
    $this->drupalGet('node/add/page');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Title'));
    $this->assertSession()->pageTextContains(t('Body'));
    $this->drupalCreateNode(['type' => 'page']);

    // Node flex page
    $this->drupalGet('node/add/flex_page');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Title'));
    $this->assertSession()->pageTextContains(t('Body'));
    $this->drupalCreateNode(['type' => 'flex_page']);
  }

}
