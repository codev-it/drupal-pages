<?php

/** @noinspection PhpUnused */

namespace Drupal\Tests\codev_pages\Functional;

use Behat\Mink\Exception\ExpectationException;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: StandardTest.php
 * .
 */

/**
 * Class StandardTest.
 *
 * Test basic class for functionality tests.
 *
 * Simple test with the basic settings based on the standard profile and
 * template for further specific function test.
 *
 * @package      Drupal\Tests\codev_pages\Functional
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class StandardTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Standard test.
   *
   * @throws ExpectationException
   */
  public function testStandard() {
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

}
