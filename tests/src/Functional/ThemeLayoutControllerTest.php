<?php

/** @noinspection PhpUnused */

namespace Drupal\Tests\codev_pages\Functional;

use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: ThemeLayoutControllerTest.php
 * .
 */

/**
 * Class ThemeLayoutControllerTest.
 *
 * @package      Drupal\Tests\codev_pages\Functional
 *
 * @group        codev_pages
 *
 * @noinspection PhpUnused
 */
class ThemeLayoutControllerTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_pages',
  ];

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $builderUser;

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $presenterUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'configure any layout',
      'administer node display',
    ]);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->builderUser = $this->drupalCreateUser([
      'configure all page node layout overrides',
      'administer node display',
    ]);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->presenterUser = $this->drupalCreateUser([
      'configure all page node layout overrides',
    ]);

  }

  /**
   * Tests the functionality.
   *
   * @throws ExpectationException
   * @throws ResponseTextException
   */
  public function testThemeLayoutController() {
    $this->rebuildAll();

    // Check admin form permission.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/appearance/layout');
    $this->assertSession()->pageTextContains(t('Global layout'));
    $this->assertSession()->pageTextNotContains(t('Layout builder global'));
    $this->assertSession()->statusCodeEquals(200);

    // Check builder form permission.
    $this->drupalLogin($this->builderUser);
    $this->drupalGet('admin/appearance/layout');
    $this->assertSession()->pageTextContains(t('Global layout'));
    $this->assertSession()->pageTextNotContains(t('Layout builder global'));
    $this->assertSession()->statusCodeEquals(200);

    // Check builder form permission.
    $this->drupalLogin($this->presenterUser);
    $this->drupalGet('admin/appearance/layout');
    $this->assertSession()->statusCodeEquals(403);

    // Check anonymous form permission.
    $this->drupalLogout();
    $this->drupalGet('admin/appearance/layout');
    $this->assertSession()->statusCodeEquals(403);
  }

}
