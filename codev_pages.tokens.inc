<?php

use Drupal\codev_pages\Helper\Paragraph;
use Drupal\codev_utils\Helper\Content;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\node\NodeInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_pages.tokens.inc
 * .
 */

/**
 * Implements hook_token_info().
 *
 * @noinspection PhpUnused
 */
function codev_pages_token_info(): array {
  return [
    'types'  => [
      'codev_pages' => [
        'name'        => t('Codev-IT Pages'),
        'description' => t('Tokens provided by the codev pages module.'),
      ],
    ],
    'tokens' => [
      'codev_pages' => [
        'body_flex_summary:?' => [
          'name'        => t('Flex body Summary'),
          'description' => t('The summary of the nodes with body flex field.'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];
  if ($type == 'codev_pages' && !empty($data['node'])) {
    /** @var NodeInterface $node */
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      $name_s = explode(':', $name);
      if ($name_s[0] == 'body_flex_summary') {
        $name_s_last = end($name_s) ?: '?';
        $length = $name_s_last !== '?' && $name_s_last !== $name_s[0]
          ? intval($name_s_last) : 260;
        /** @noinspection PhpUnhandledExceptionInspection */
        $body_summery = $node->hasField('body_summary')
        && !$node->get('body_summary')->isEmpty()
          ? $node->get('body_summary')->first()->getValue()['value'] : '';
        if (empty($body_summery) && $node->hasField('body_flex')) {
          /** @var EntityReferenceRevisionsFieldItemList $body_flex_field */
          $body_flex_field = $node->get('body_flex');
          $body_flex_entities = $body_flex_field->referencedEntities() ?: [];
          $text = Paragraph::parseTextMultiple($body_flex_entities);
          $body_summery = Content::subStrByWords($text, $length);
        }
        $replacements[$original] = $body_summery;
      }
    }
  }

  return $replacements;
}
