<?php

use Drupal\codev_pages\Settings;
use Drupal\codev_utils\Helper\Form;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Form\DefaultsEntityForm;
use Drupal\layout_builder\Form\LayoutBuilderEntityViewDisplayForm;
use Drupal\layout_builder\Form\OverridesEntityForm;
use Drupal\views\ViewExecutable;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_pages.forms.inc
 * .
 */

/**
 * Implements hook_views_exposed_form().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  /** @var ViewExecutable $view */
  $view = $form_state->getStorage()['view'];
  if ($view->id() == 'content' && isset($form['type']['#options'][Settings::THEME_LAYOUT_NODE_TYPE])) {
    unset($form['type']['#options'][Settings::THEME_LAYOUT_NODE_TYPE]);
  }
}

/**
 * Implements hook_form_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  /** @var DefaultsEntityForm $form_object */
  $form_object = $form_state->getFormObject();
  $if_defaults_entity_form = $form_object instanceof DefaultsEntityForm;
  if ($if_defaults_entity_form) {
    /** @var Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay $entity */
    $entity = $form_object->getEntity();
    if ($entity->getTargetEntityTypeId() === 'node'
      && $entity instanceof LayoutBuilderEntityViewDisplay) {
      $form_state->setRebuild();
      $destination = Drupal::request()->query->get('destination');
      $form_state->set('destination', $destination);
      $form['actions']['submit']['#submit'][] = '_codev_pages_form_layout_builder_redirect_submit';
    }
  }

  if ($if_defaults_entity_form || $form_object instanceof OverridesEntityForm) {
    $form['actions']['#weight'] = 999;
  }
}

/**
 * Layout builder submit redirect handler.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_pages_form_layout_builder_redirect_submit(array $form, FormStateInterface $form_state): void {
  $destination = !empty($form_state->get('destination'))
    ? preg_replace('/^\//m', '', $form_state->get('destination'))
    : '';
  $destination_url = !empty($destination) ?
    Drupal::service('path.validator')->getUrlIfValid($destination) : NULL;
  if (!empty($destination_url) && $destination_url->isRouted()) {
    $form_state->setRedirect($destination_url->getRouteName());
  }
  else {
    $current_user = Drupal::currentUser();
    if (!$current_user->hasPermission(Settings::PERM_ADMIN_NODE_TYPES)) {
      $form_state->setRedirect('codev_pages.theme_layout_builder');
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_form_node_form_alter(&$form, FormStateInterface $form_state): void {
  if (!empty($form['body_summary']) && !empty($form['body_flex'])) {
    $form['#attached']['library'][] = 'codev_pages/body-flex-summery';
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_form_layout_paragraphs_component_form_alter(&$form, FormStateInterface $form_state): void {
  $form['layout_paragraphs']['#process'][] = '_codev_pages_layout_paragraphs_component_hide_select_by_single_option';
}

/**
 * Form #process callback.
 *
 * Renders the layout paragraphs behavior form for layout selection.
 *
 * @param array                                $element
 *   The form element.
 * @param FormStateInterface $form_state
 *   The form state.
 * @param array                                $form
 *   The complete form array.
 *
 * @return array
 *   The processed element.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_pages_layout_paragraphs_component_hide_select_by_single_option(array $element, FormStateInterface $form_state, array &$form): array {
  if (isset($element['layout']['#options'])) {
    if (count($element['layout']['#options']) === 1) {
      $element['layout']['#access'] = FALSE;
      $element['config']['#type'] = 'container';
    }
  }
  return $element;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 */
function codev_pages_form_entity_view_display_edit_form_alter(&$form, FormStateInterface $form_state): void {
  /** @var LayoutBuilderEntityViewDisplayForm $form_object */
  $form_object = $form_state->getFormObject();
  /** @var Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay $entity */
  $entity = $form_object->getEntity();
  if ($entity->getTargetEntityTypeId() === 'node') {
    $access = Drupal::currentUser()
      ->hasPermission(Settings::PERM_ADMIN_NODE_TYPES);
    $accepts = ['modes', 'refresh', 'actions', 'layout'];
    foreach ($form as $key => $field) {
      if (in_array($key, $accepts) && is_array($field)) {
        $form[$key]['#access'] = $access;
      }
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_pages_field_widget_form_alter(&$element, FormStateInterface $form_state, $context): void {
  $items = $context['items'];
  $field_definition = $items->getFieldDefinition();
  if ($field_definition instanceof FieldConfigBase) {
    if ($field_definition->getTargetEntityTypeId() === 'paragraph') {
      $parent_array = Utils::getArrayValue('#field_parents', $element, []);
      switch ($field_definition->getTargetBundle()) {
        case'image':
          if ($field_definition->get('field_name') == 'field_link') {
            $parent_array[] = 'field_colorbox';
            $field_name = Form::buildFormItemName($parent_array);
            $input_field = sprintf(':input[name="%s"]', $field_name);
            $element['uri']['#states'] = [
              'visible' => [$input_field => ['value' => 'false']],
            ];
          }
          break;
        case 'icon':
          $parent_array[] = 'field_select_icon_type';
          $field_name = Form::buildFormItemName($parent_array);
          $input_field = sprintf(':input[name="%s"]', $field_name);
          switch ($field_definition->get('field_name')) {
            case 'field_font_awesome':
              $element['#type'] = 'container';
              $element['#states'] = [
                'visible' => [$input_field => ['value' => 'font_awesome']],
              ];
              break;
            case 'field_image':
              $element['#states'] = [
                'visible' => [$input_field => ['value' => 'image']],
              ];
              break;
          }
          break;
      }
    }
  }
}
