// noinspection DuplicatedCode,JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(($, Drupal, Sortable, once) => {
  const { behaviors, ajax } = Drupal;

  /**
   * Section region selector.
   *
   * @type {string}
   */
  const regionSelector = '.js-layout-builder-region-section';

  /**
   * Get all nested section and return the highest delta.
   *
   * @param {Object} $items JQuery items
   * @param {number} currIndex Current item index
   *
   * @returns {number} Return the last nested section item number
   */
  const getLastNestedDelta = ($items, currIndex) => {
    let ret = -1;
    for (let i = 0; i < currIndex; i++) {
      const $nestedItems = $items.eq(i).find('[data-layout-delta]');
      // eslint-disable-next-line no-loop-func
      $nestedItems.each((key, nestedItem) => {
        const delta = Number(nestedItem.dataset.layoutDelta);
        if (delta > ret) {
          ret = delta;
        }
      });
    }
    return ret;
  };

  /**
   * Section the last delta from the parent element.
   *
   * @param {string} parent Parent name sting
   * @param {string} currItem Current name sting
   *
   * @returns {*} Return item delta id
   */
  const getLastParentSectionDelta = (parent, currItem) => {
    const $parent = $(parent);
    let ret = $parent.closest('[data-layout-delta]').data('layout-delta');
    const $items = $parent.find('.layout-builder__section');
    $items.each((i, elem) => {
      if (elem === currItem && i !== 0) {
        const $prevItem = $items.eq(i - 1);
        ret = $prevItem.find('[data-layout-delta]').data('layout-delta');
      }
    });
    return ret;
  };

  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesLayoutBuilderSectionDrag = {
    attach: () => {
      if (!once('layout-builder-section-drag', regionSelector).length) {
        return;
      }

      Array.prototype.forEach.call(
        document.querySelectorAll(regionSelector),
        (region) => {
          region.addEventListener('mouseover', (event) => {
            const elem = event.target;
            elem.classList.add('dragging');
          });

          region.addEventListener('mouseout', (event) => {
            const elem = event.target;
            elem.classList.remove('dragging');
          });

          Sortable.create(region, {
            draggable: '.layout-builder__section',
            handle: '.layout-builder__draggable-section',
            ghostClass: 'ui-state-drop',
            group: 'builder-region',
            onEnd: (event) => {
              return Drupal.layoutBuilderSectionUpdate(
                event.item,
                event.from,
                event.to
              );
            }
          });
        }
      );
    }
  };

  /**
   * Darg event handler for update section infos.
   *
   * @param {string} item Current item name
   * @param {string} from From item name
   * @param {string} to To item name
   */
  Drupal.layoutBuilderSectionUpdate = (item, from, to) => {
    const $item = $(item);
    const $from = $(from);
    const $itemRegion = $item.closest(regionSelector);

    if (to === $itemRegion[0] && $from.length) {
      const deltaFrom = $item.find('[data-layout-delta]').data('layout-delta');
      let deltaTo = deltaFrom;
      let weight;
      const parentTo = $itemRegion
        .closest('[data-layout-delta]')
        .data('layout-delta');
      let $items;
      let updateWeight = true;
      const isParentTo = parentTo !== undefined;

      if (!isParentTo) {
        $items = $(`#layout-builder > .layout-builder__section`);
      } else {
        $items = $itemRegion.find('> div:not(.layout-builder__add-block)');
      }

      $items.each((i, elem) => {
        const isCurrentItem = item === elem;
        if (!isParentTo) {
          if (isCurrentItem) {
            deltaTo = getLastNestedDelta($items, i);
            if (deltaFrom > deltaTo) {
              deltaTo += 1;
            }
          }
        } else {
          if (isCurrentItem) {
            deltaTo = getLastParentSectionDelta(to, item) + 1;
            updateWeight = false;
          }
          if (weight === undefined) {
            weight = 0;
          } else if (updateWeight) {
            weight += 1;
          }
        }
      });

      ajax({
        url: [
          $item.data('layout-update-section-url'),
          deltaFrom,
          deltaTo,
          parentTo,
          $itemRegion.data('region'),
          weight
        ]
          .filter((element) => element !== undefined)
          .join('/')
      }).execute();
    }
  };

  /**
   * Callback used in {@link Drupal.behaviors.layoutBuilderBlockDrag}.
   *
   * @param {HTMLElement} item
   *   The HTML element representing the repositioned block.
   * @param {HTMLElement} from
   *   The HTML element representing the previous parent of item
   * @param {HTMLElement} to
   *   The HTML element representing the current parent of item
   *
   * @internal This method is a callback for layoutBuilderBlockDrag and is used
   *  in FunctionalJavascript tests. It may be renamed if the test changes.
   *  @see https://www.drupal.org/node/3084730
   */
  Drupal.layoutBuilderBlockUpdate = (item, from, to) => {
    const $item = $(item);
    const $from = $(from);

    // Check if the region from the event and region for the item match.
    const itemRegion = $item.closest('.js-layout-builder-region');
    if (to === itemRegion[0]) {
      // Find the destination delta.
      const deltaTo = $item.closest('[data-layout-delta]').data('layout-delta');
      // If the block didn't leave the original delta use the destination.
      const deltaFrom = $from
        ? $from.closest('[data-layout-delta]').data('layout-delta')
        : deltaTo;
      ajax({
        url: [
          $item.closest('[data-layout-update-url]').data('layout-update-url'),
          deltaFrom,
          deltaTo,
          itemRegion.data('region'),
          $item.data('layout-block-uuid'),
          $item.prevAll('[data-layout-block-uuid]').data('layout-block-uuid')
        ]
          .filter((element) => element !== undefined)
          .join('/')
      }).execute();
    }
  };
})(jQuery, Drupal, Sortable, once);
