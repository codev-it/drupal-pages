// noinspection All

/**
 * @file
 * Behaviors of codev_pages module.
 */
(function ($, Drupal, Sortable, once) {
  var behaviors = Drupal.behaviors,
    ajax = Drupal.ajax;

  /**
   * Section region selector.
   *
   * @type {string}
   */
  var regionSelector = '.js-layout-builder-region-section';

  /**
   * Get all nested section and return the highest delta.
   *
   * @param {Object} $items JQuery items
   * @param {number} currIndex Current item index
   *
   * @returns {number} Return the last nested section item number
   */
  var getLastNestedDelta = function getLastNestedDelta($items, currIndex) {
    var ret = -1;
    for (var i = 0; i < currIndex; i++) {
      var $nestedItems = $items.eq(i).find('[data-layout-delta]');
      // eslint-disable-next-line no-loop-func
      $nestedItems.each(function (key, nestedItem) {
        var delta = Number(nestedItem.dataset.layoutDelta);
        if (delta > ret) {
          ret = delta;
        }
      });
    }
    return ret;
  };

  /**
   * Section the last delta from the parent element.
   *
   * @param {string} parent Parent name sting
   * @param {string} currItem Current name sting
   *
   * @returns {*} Return item delta id
   */
  var getLastParentSectionDelta = function getLastParentSectionDelta(parent, currItem) {
    var $parent = $(parent);
    var ret = $parent.closest('[data-layout-delta]').data('layout-delta');
    var $items = $parent.find('.layout-builder__section');
    $items.each(function (i, elem) {
      if (elem === currItem && i !== 0) {
        var $prevItem = $items.eq(i - 1);
        ret = $prevItem.find('[data-layout-delta]').data('layout-delta');
      }
    });
    return ret;
  };

  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesLayoutBuilderSectionDrag = {
    attach: function attach() {
      if (!once('layout-builder-section-drag', regionSelector).length) {
        return;
      }
      Array.prototype.forEach.call(document.querySelectorAll(regionSelector), function (region) {
        region.addEventListener('mouseover', function (event) {
          var elem = event.target;
          elem.classList.add('dragging');
        });
        region.addEventListener('mouseout', function (event) {
          var elem = event.target;
          elem.classList.remove('dragging');
        });
        Sortable.create(region, {
          draggable: '.layout-builder__section',
          handle: '.layout-builder__draggable-section',
          ghostClass: 'ui-state-drop',
          group: 'builder-region',
          onEnd: function onEnd(event) {
            return Drupal.layoutBuilderSectionUpdate(event.item, event.from, event.to);
          }
        });
      });
    }
  };

  /**
   * Darg event handler for update section infos.
   *
   * @param {string} item Current item name
   * @param {string} from From item name
   * @param {string} to To item name
   */
  Drupal.layoutBuilderSectionUpdate = function (item, from, to) {
    var $item = $(item);
    var $from = $(from);
    var $itemRegion = $item.closest(regionSelector);
    if (to === $itemRegion[0] && $from.length) {
      var deltaFrom = $item.find('[data-layout-delta]').data('layout-delta');
      var deltaTo = deltaFrom;
      var weight;
      var parentTo = $itemRegion.closest('[data-layout-delta]').data('layout-delta');
      var $items;
      var updateWeight = true;
      var isParentTo = parentTo !== undefined;
      if (!isParentTo) {
        $items = $("#layout-builder > .layout-builder__section");
      } else {
        $items = $itemRegion.find('> div:not(.layout-builder__add-block)');
      }
      $items.each(function (i, elem) {
        var isCurrentItem = item === elem;
        if (!isParentTo) {
          if (isCurrentItem) {
            deltaTo = getLastNestedDelta($items, i);
            if (deltaFrom > deltaTo) {
              deltaTo += 1;
            }
          }
        } else {
          if (isCurrentItem) {
            deltaTo = getLastParentSectionDelta(to, item) + 1;
            updateWeight = false;
          }
          if (weight === undefined) {
            weight = 0;
          } else if (updateWeight) {
            weight += 1;
          }
        }
      });
      ajax({
        url: [$item.data('layout-update-section-url'), deltaFrom, deltaTo, parentTo, $itemRegion.data('region'), weight].filter(function (element) {
          return element !== undefined;
        }).join('/')
      }).execute();
    }
  };

  /**
   * Callback used in {@link Drupal.behaviors.layoutBuilderBlockDrag}.
   *
   * @param {HTMLElement} item
   *   The HTML element representing the repositioned block.
   * @param {HTMLElement} from
   *   The HTML element representing the previous parent of item
   * @param {HTMLElement} to
   *   The HTML element representing the current parent of item
   *
   * @internal This method is a callback for layoutBuilderBlockDrag and is used
   *  in FunctionalJavascript tests. It may be renamed if the test changes.
   *  @see https://www.drupal.org/node/3084730
   */
  Drupal.layoutBuilderBlockUpdate = function (item, from, to) {
    var $item = $(item);
    var $from = $(from);

    // Check if the region from the event and region for the item match.
    var itemRegion = $item.closest('.js-layout-builder-region');
    if (to === itemRegion[0]) {
      // Find the destination delta.
      var deltaTo = $item.closest('[data-layout-delta]').data('layout-delta');
      // If the block didn't leave the original delta use the destination.
      var deltaFrom = $from ? $from.closest('[data-layout-delta]').data('layout-delta') : deltaTo;
      ajax({
        url: [$item.closest('[data-layout-update-url]').data('layout-update-url'), deltaFrom, deltaTo, itemRegion.data('region'), $item.data('layout-block-uuid'), $item.prevAll('[data-layout-block-uuid]').data('layout-block-uuid')].filter(function (element) {
          return element !== undefined;
        }).join('/')
      }).execute();
    }
  };
})(jQuery, Drupal, Sortable, once);
