// noinspection JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(function ($, Drupal, once) {
  var behaviors = Drupal.behaviors;

  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesBodyFlexSummery = {
    attach: function attach(context) {
      var bodyFlex = once('flex-body-summery', '.field--name-body-flex', context);
      var bodyFlexSummery = once('flex-body-summery', '.field--name-body-summary', context);
      if (!bodyFlexSummery.length && !bodyFlex.length) {
        return;
      }
      var $bodyFlex = $(bodyFlex);
      var $bodyFlexSummery = $(bodyFlexSummery);
      if ($bodyFlex.length && $bodyFlexSummery.length) {
        var $bodyFlexLegend = $bodyFlex.find('legend .fieldset__label').eq(0);
        var $bodyFlexSummeryLabel = $bodyFlexSummery.find('label').eq(0);
        var $summeryTextarea = $bodyFlexSummery.find('textarea');
        var textShow = Drupal.t('Edit summary');
        var textHide = Drupal.t('Hide summary');
        var $link = $("<span class=\"field-edit-link\">\n            (<button type=\"button\" class=\"link link-edit-summary\">\n              <strong>".concat(textHide, "</strong>\n            </button>)\n          </span>"));
        var $button = $link.find('button');
        var toggleClick = true;
        $link.on('click', function (e) {
          if (toggleClick) {
            $bodyFlexSummery.hide();
            $button.html(textShow);
            $link.appendTo($bodyFlexLegend);
          } else {
            $bodyFlexSummery.show();
            $link.appendTo($bodyFlexSummeryLabel);
          }
          e.preventDefault();
          toggleClick = !toggleClick;
        }).appendTo($bodyFlexSummeryLabel);

        // If no summary is set, hide the summary field.
        if ($summeryTextarea.val() === '') {
          $link.trigger('click');
        }
      }
    }
  };
})(jQuery, Drupal, once);
