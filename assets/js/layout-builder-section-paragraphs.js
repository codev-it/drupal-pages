// noinspection All

/**
 * @file
 * Behaviors of codev_pages module.
 */
(function ($, Drupal) {
  var behaviors = Drupal.behaviors;
  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */

  behaviors.codevPagesLayoutBuilderSectionParagraphs = {
    attach: function attach(context) {
      var $dialog = $(context).find('.ui-dialog').once('layout-builder-section-paragraphs');
      var $columns = $dialog.find('.layout-builder-select-grid-layout').once('layout-builder-section-paragraphs');

      if ($columns.length && !$columns.find('input[type="radio"]:checked').length) {
        var $submit = $dialog.find('.form-submit:not(.button--danger)');
        var $columnsLabel = $columns.find('.fieldset-legend');
        var $columnsRadios = $columns.find('input[type="radio"]');
        $submit.addClass('disabled').attr('disabled', 'disabled');
        $columnsLabel.addClass('form-required');
        $columnsRadios.on('change', function () {
          $submit.removeClass('disabled').removeAttr('disabled');
        });
      }
    }
  };
})(jQuery, Drupal);
