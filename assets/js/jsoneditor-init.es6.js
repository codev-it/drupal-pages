// noinspection DuplicatedCode,JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(($, Drupal, JSONEditor, once) => {
  const { behaviors } = Drupal;

  /**
   * Init json editor behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesInitJsonEditor = {
    attach: () => {
      const editorWrp = {};
      const container = document.getElementsByClassName('json-editor');
      once('json-editor', container).forEach((elem, i) => {
        const parent = elem.parentElement;
        const active = Boolean(parent.getAttribute('data-json-editor'));
        if (!active) {
          const options = {
            search: false,
            onChangeText: (jsonString) => {
              elem.value = jsonString;
            }
          };
          editorWrp[i] = document.createElement('div');
          editorWrp[i].id = `json-editor-${i}`;
          const editor = new JSONEditor(editorWrp[i], options);
          const json =
            elem.value !== '' ? JSON.parse(elem.value) : { class: [] };
          editor.set(json);
          elem.setAttribute('style', 'display: none;');
          parent.appendChild(editorWrp[i]);
          parent.setAttribute('data-json-editor', 'true');
        }
      });
    }
  };
})(jQuery, Drupal, JSONEditor, once);
