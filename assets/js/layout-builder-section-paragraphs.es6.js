// noinspection JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(($, Drupal) => {
  const { behaviors } = Drupal;

  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesLayoutBuilderSectionParagraphs = {
    attach: (context) => {
      const $dialog = $(context)
        .find('.ui-dialog')
        .once('layout-builder-section-paragraphs');
      const $columns = $dialog
        .find('.layout-builder-select-grid-layout')
        .once('layout-builder-section-paragraphs');
      if (
        $columns.length &&
        !$columns.find('input[type="radio"]:checked').length
      ) {
        const $submit = $dialog.find('.form-submit:not(.button--danger)');
        const $columnsLabel = $columns.find('.fieldset-legend');
        const $columnsRadios = $columns.find('input[type="radio"]');
        $submit.addClass('disabled').attr('disabled', 'disabled');
        $columnsLabel.addClass('form-required');
        $columnsRadios.on('change', () => {
          $submit.removeClass('disabled').removeAttr('disabled');
        });
      }
    }
  };
})(jQuery, Drupal);
