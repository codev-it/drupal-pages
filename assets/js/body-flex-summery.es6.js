// noinspection JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(($, Drupal, once) => {
  const { behaviors } = Drupal;

  /**
   * Layout builder section drag behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesBodyFlexSummery = {
    attach: (context) => {
      const bodyFlex = once('flex-body-summery', '.field--name-body-flex', context);
      const bodyFlexSummery = once('flex-body-summery', '.field--name-body-summary', context);

      if (!bodyFlexSummery.length && !bodyFlex.length) {
        return;
      }

      const $bodyFlex = $(bodyFlex);
      const $bodyFlexSummery = $(bodyFlexSummery);
      if ($bodyFlex.length && $bodyFlexSummery.length) {
        const $bodyFlexLegend = $bodyFlex.find('legend .fieldset__label').eq(0);
        const $bodyFlexSummeryLabel = $bodyFlexSummery.find('label').eq(0);
        const $summeryTextarea = $bodyFlexSummery.find('textarea');

        const textShow = Drupal.t('Edit summary');
        const textHide = Drupal.t('Hide summary');

        const $link = $(
          `<span class="field-edit-link">
            (<button type="button" class="link link-edit-summary">
              <strong>${textHide}</strong>
            </button>)
          </span>`
        );
        const $button = $link.find('button');
        let toggleClick = true;
        $link
          .on('click', (e) => {
            if (toggleClick) {
              $bodyFlexSummery.hide();
              $button.html(textShow);
              $link.appendTo($bodyFlexLegend);
            } else {
              $bodyFlexSummery.show();
              $link.appendTo($bodyFlexSummeryLabel);
            }
            e.preventDefault();
            toggleClick = !toggleClick;
          })
          .appendTo($bodyFlexSummeryLabel);

        // If no summary is set, hide the summary field.
        if ($summeryTextarea.val() === '') {
          $link.trigger('click');
        }
      }
    }
  };
})(jQuery, Drupal, once);
