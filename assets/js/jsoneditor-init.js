// noinspection DuplicatedCode,JSValidateTypes

/**
 * @file
 * Behaviors of codev_pages module.
 */
(function ($, Drupal, JSONEditor, once) {
  var behaviors = Drupal.behaviors;

  /**
   * Init json editor behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevPagesInitJsonEditor = {
    attach: function attach() {
      var editorWrp = {};
      var container = document.getElementsByClassName('json-editor');
      once('json-editor', container).forEach(function (elem, i) {
        var parent = elem.parentElement;
        var active = Boolean(parent.getAttribute('data-json-editor'));
        if (!active) {
          var options = {
            search: false,
            onChangeText: function onChangeText(jsonString) {
              elem.value = jsonString;
            }
          };
          editorWrp[i] = document.createElement('div');
          editorWrp[i].id = "json-editor-".concat(i);
          var editor = new JSONEditor(editorWrp[i], options);
          var json = elem.value !== '' ? JSON.parse(elem.value) : {
            "class": []
          };
          editor.set(json);
          elem.setAttribute('style', 'display: none;');
          parent.appendChild(editorWrp[i]);
          parent.setAttribute('data-json-editor', 'true');
        }
      });
    }
  };
})(jQuery, Drupal, JSONEditor, once);
